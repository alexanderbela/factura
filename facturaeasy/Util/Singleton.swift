


import Foundation


class Singleton {
    
    static let shared = Singleton()
    
    var imageFac: dataUser?
    var textImag: textImageModel?
    var isValid: isValidPayment?
    var types: TypesDataModel?
    var cfdi: CFDIDataModel?
    var payment: PaymentDataModel?
    var companies: CompaniesDataModel?
    var cantidadFacturas: AmountFacturasDataModel?
    //var currentSessionKey: SessionKey?
    
    
     private init() {
        //don't forget to make this private
    }
    
   
    
    func getImageTicketGallery()-> dataUser?{
        return imageFac
    }
    
    func getTextFromImage()-> textImageModel?{
        return textImag
    }
    
    func getIsValidPayment ()-> isValidPayment?{
        return isValid
    }
    
    func getTypes ()-> TypesDataModel?{
        return types
    }
    
    func getCFDI ()-> CFDIDataModel?{
        return cfdi
    }
    
    func getPayment ()-> PaymentDataModel?{
        return payment
    }
    
    func getCompanies ()-> CompaniesDataModel?{
        return companies
    }
    
    func getCantidadFacturas ()-> AmountFacturasDataModel?{
        return cantidadFacturas
    }
    
    func setCantidadFacturas(name: AmountFacturasDataModel){
        cantidadFacturas = name
    }
    
    func setCompanies(name: CompaniesDataModel){
        companies = name
    }
    
    func setPayment(name: PaymentDataModel){
        payment = name
    }
    
    func setCFDI(name: CFDIDataModel){
        cfdi = name
    }
    
    func setTypes(name: TypesDataModel){
        types = name
    }
    
    func setIsValidPayment(name: isValidPayment){
        isValid = name
    }
    
    
    func setImageTicketGallery(name: dataUser){
        imageFac = name
    }
    
    func setTextFromImage(name: textImageModel){
        textImag = name
    }
    
    
    
    
}
