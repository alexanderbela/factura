//
//  SaveDataViewController.swift
//  facturaeasy
//
//  Created by Baxten on 17/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Firebase

extension Notification.Name {
    static let addNewProfile = Notification.Name(
        rawValue: "UserPassengerAdded")
    
}

class ProfileDataViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var pickerViewType: UIPickerView!
    @IBOutlet weak var aliasTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var secondLastNameTextField: UITextField!
    @IBOutlet weak var rfcTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var municipioTextField: UITextField!
    @IBOutlet weak var direccionTextField: UITextField!
    @IBOutlet weak var cpTextField: UITextField!
    @IBOutlet weak var useCFDIDataPicker: UITextField!
    @IBOutlet weak var paymentMethodDataPicker: UITextField!
    @IBOutlet weak var negocioRecurrenteDataPicker: UITextField!
    @IBOutlet weak var tipoDataPicker: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var saveButtonSaved: UIButton!
    @IBOutlet weak var imageOne: UIButton!
    @IBOutlet weak var backgroundImageView: UIView!
    @IBOutlet var viewPrincipal: UIView!
    @IBOutlet weak var viewWithFisica: UIView!
    @IBOutlet weak var viewWithMoral: UIView!
    @IBOutlet weak var viewWithTypes: UIView!
    @IBOutlet weak var RazonSocialMoralTextField: UITextField!
    
    
    
    var pickerView = UIPickerView()
    var pickerViewMethods = UIPickerView()
    var pickerViewCFDI = UIPickerView()
    var pickerViewCompanies = UIPickerView()
    
    //var selectedIndex:IndexPath?
    var selectedType: String?
    var selectedTypeMethod: String?
    var selectedCFDI: String?
    var selectedCompanies: String?
    
    var selectedTypeRow: Int = 0
    var selectedMethodRow: Int = 0
    var selectedCFDIRow: Int = 0
    var selectedCompaniesRow: Int = 0
    
    var rowSelected: String = ""
    
    var images: [String] = [
        "ic_profile_beach_button", "ic_profile_city_button", "ic_profile_mountain_button",
        "ic_profile_pyramid_button", "ic_profile_snow_button", "ic_profile_space_button"
    ]
    
    var db: Firestore!
    
    var typesCount: Int = 0;
    
    var selectedIndex: Int = 0
    
    var values = [JSON]()
    var validation = Validation()
    
    var profileArray = [ProfileDataModel]()
    var TypesDataModelArray = [TypesDataModel]()
    
    var typesCompanies = Singleton.shared.getCompanies()?.companies
    var types = Singleton.shared.getTypes()?.types
    var typesCFDI = Singleton.shared.getCFDI()?.cfdi
    var typesMethods = Singleton.shared.getPayment()?.payment
    
    
    override func viewDidLoad() {
        
        db = Firestore.firestore()
        
        createPickerView()
        dismissPickerView()
        
        collectionView.delegate = self;
        collectionView.dataSource = self;
        
        self.dismissKey()
        
        iconsTextField()
        //phoneTextField.keyboardType = .numberPad
        //cpTextField.keyboardType = .numberPad
        print("entra a viewDidLoad ProfileDataViewController")
        
        self.negocioRecurrenteDataPicker.text = self.typesCompanies?.first
        self.selectedCompanies = self.typesCompanies?.first
        self.tipoDataPicker.text = self.types?.first
        self.useCFDIDataPicker.text = self.typesCFDI?.first
        self.paymentMethodDataPicker.text = self.typesMethods?.first
        self.viewWithTypes.bringSubviewToFront(self.viewWithFisica)
        self.viewWithTypes.sendSubviewToBack(self.viewWithMoral)
        self.selectedType = self.types![0]
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        // Additional code here
        return false
    }
    
    func iconsTextField(){
        
        tipoDataPicker.rightViewMode = UITextField.ViewMode.always
        tipoDataPicker.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        paymentMethodDataPicker.rightViewMode = UITextField.ViewMode.always
        paymentMethodDataPicker.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        useCFDIDataPicker.rightViewMode = UITextField.ViewMode.always
        useCFDIDataPicker.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        negocioRecurrenteDataPicker.rightViewMode = UITextField.ViewMode.always
        negocioRecurrenteDataPicker.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
    }
    
    
    func createPickerView() {
        
        pickerView.delegate = self
        tipoDataPicker.inputView = pickerView
        
        pickerViewMethods.delegate = self
        paymentMethodDataPicker.inputView = pickerViewMethods
        
        pickerViewCFDI.delegate = self
        useCFDIDataPicker.inputView = pickerViewCFDI
        
        pickerViewCompanies.delegate = self
        negocioRecurrenteDataPicker.inputView = pickerViewCompanies
        
    }
    
    func dismissPickerView() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        tipoDataPicker.inputAccessoryView = toolBar
        
        let toolBarMethod = UIToolbar()
        toolBarMethod.sizeToFit()
        let buttonMethod = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarMethod.setItems([buttonMethod], animated: true)
        toolBarMethod.isUserInteractionEnabled = true
        paymentMethodDataPicker.inputAccessoryView = toolBarMethod
        
        let toolBarCFDI = UIToolbar()
        toolBarCFDI.sizeToFit()
        let buttonCFDI = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarCFDI.setItems([buttonCFDI], animated: true)
        toolBarCFDI.isUserInteractionEnabled = true
        useCFDIDataPicker.inputAccessoryView = toolBarCFDI
        
        let toolBarCompanies = UIToolbar()
        toolBarCompanies.sizeToFit()
        let buttonCompanies = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarCompanies.setItems([buttonCompanies], animated: true)
        toolBarCompanies.isUserInteractionEnabled = true
        negocioRecurrenteDataPicker.inputAccessoryView = toolBarCompanies
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("entra a viewWillAppear ProfileDataViewController");
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.pickerView {
            typesCount = types!.count
        }
        
        if pickerView == self.pickerViewMethods {
            typesCount = typesMethods!.count
        }
        
        if pickerView == self.pickerViewCFDI {
            typesCount = typesCFDI!.count
        }
        
        if pickerView == self.pickerViewCompanies {
            typesCount = typesCompanies!.count
        }
        
        return typesCount
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.pickerView {
            rowSelected = types![row]
        }
        
        if pickerView == self.pickerViewMethods {
            rowSelected = typesMethods![row]
        }
        
        if pickerView == self.pickerViewCFDI {
            rowSelected = typesCFDI![row]
        }
        
        if pickerView == self.pickerViewCompanies {
            rowSelected = typesCompanies![row]
        }
        
        return rowSelected
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerView {
            selectedTypeRow = row
            selectedType = types![row] // selected item
            tipoDataPicker.text = selectedType
            print("selectedType pickerView: \(selectedType!)")
            
            if selectedType == "Persona física"{
                self.viewWithTypes.bringSubviewToFront(self.viewWithFisica)
                self.viewWithTypes.sendSubviewToBack(self.viewWithMoral)
            }else{
                self.viewWithTypes.bringSubviewToFront(self.viewWithMoral)
                self.viewWithTypes.sendSubviewToBack(self.viewWithFisica)
            }
        }
        
        if pickerView == pickerViewMethods {
            selectedMethodRow = row
            selectedTypeMethod = typesMethods![row] // selected item
            paymentMethodDataPicker.text = selectedTypeMethod
            print("selectedType pickerViewMethods: \(selectedTypeMethod!)")
        }
        
        if pickerView == pickerViewCFDI {
            selectedCFDIRow = row
            selectedCFDI = typesCFDI![row] // selected item
            useCFDIDataPicker.text = selectedCFDI
            print("selectedType pickerViewCFDI: \(selectedCFDI!)")
        }
        
        if pickerView == pickerViewCompanies {
            //selectedCompaniesRow = row
            selectedCompanies = typesCompanies![row] // selected item
            negocioRecurrenteDataPicker.text = selectedCompanies
            print("selectedType pickerViewCompanies: \(selectedCompanies!)")
        }
        
    }
    
    
    @IBAction func saveButtonInit(_ sender: UIButton) {
        
        let userID = Auth.auth().currentUser!.uid
        let ref = db.collection("users")
        let docId = ref.document().documentID
        print("selectedType save button: \(self.selectedType!)")
        
        if self.selectedType == "Persona física"{
            if (aliasTextField.text == ""){
                showToast(message: "Por favor ingresa tu alias")
                return
            }
            
            if (nameTextField.text == ""){
                showToast(message: "Por favor ingresa tu nombre")
                return
            }
            
            if (lastNameTextField.text == ""){
                showToast(message: "Por favor ingresa tu apellido paterno")
                return
            }
            
            if (secondLastNameTextField.text == ""){
                showToast(message: "Por favor ingresa tu apellido materno")
                return
            }
            
            if (rfcTextField.text == ""){
                showToast(message: "Por favor ingresa tu RFC")
                return
            }
            
            if (rfcTextField.text!.count != 13){
                showToast(message: "El RFC de personas físicas debe ser de 13 caracteres")
                return
            }
            
            if (emailTextField.text == ""){
                showToast(message: "Por favor ingresa tu correo")
                return
            }
            
            let isValidateEmail = self.validation.validateEmailId(emailID: emailTextField.text!)
            if isValidateEmail == false {
                showToast(message: "Por favor ingresa un correo válido")
                return
            }
            
            /*
            if (phoneTextField.text == ""){
                showToast(message: "Por favor ingresa tu teléfono")
                return
            }
            
            if (phoneTextField.text?.count != 10){
                showToast(message: "El número de teléfono debe ser de 10 dígitos")
                return
            }
     */
            
            let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phoneTextField.text!)
            if isValidatePhone == false {
                showToast(message: "El número de teléfono debe ser de 10 dígitos numéricos")
                return
            }
            
            
            if (stateTextField.text == ""){
                showToast(message: "Por favor ingresa tu estado")
                return
            }
            
            if (municipioTextField.text == ""){
                showToast(message: "Por favor ingresa tu municipio")
                return
            }
            
            if (direccionTextField.text == ""){
                showToast(message: "Por favor ingresa tu dirección")
                return
            }
            
            let isValidateCP = self.validation.validaCodePostal(code: cpTextField.text!)
            if isValidateCP == false {
                showToast(message: "El código postal debe ser de 5 números")
                return
            }
            
            let docData: [String: Any] = [
                
                "alias": aliasTextField.text!,
                "address": direccionTextField.text!,
                "defaultCdfi": selectedCFDIRow,
                "defaultCompanyId": selectedCompanies!,
                "defaultPaymentMethod": selectedMethodRow,
                "email": emailTextField.text!,
                "enabled": true,
                "id": docId,
                "lastNameFather": lastNameTextField.text!,
                "lastNameMother": secondLastNameTextField.text!,
                "municipality": municipioTextField.text!,
                "name": nameTextField.text!,
                "phoneNumber": phoneTextField.text!,
                "postalCode": cpTextField.text!,
                "rfc": rfcTextField.text!,
                "selectedImagePosition": selectedIndex,
                "state": stateTextField.text!,
                "type": selectedTypeRow
                
            ]
            
            db.collection("users").document(userID).collection("profiles").document(docId).setData(docData) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                    print("docData: \(docData)")
                } else {
                    
                    self.sendAddedNewProfileObserver()
                    
                    print("Document successfully written!")
                    print("docData: \(docData)")
                    //let profileData = ProfileDataModel(data: docData)
                    //self.profileArray.append(profileData!)
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                    yourVc.modalPresentationStyle = .fullScreen
                    self.present(yourVc, animated: true, completion: nil)
                    
                    //self.dismiss(animated: true, completion: nil)
                    
                }
            }
        }else{
            
            if (aliasTextField.text == ""){
                showToast(message: "Por favor ingresa tu alias")
                return
            }
            
            
            if (RazonSocialMoralTextField.text == ""){
                showToast(message: "Por favor ingresa tu razón social")
                return
            }
            
            if (rfcTextField.text == ""){
                showToast(message: "Por favor ingresa tu RFC")
                return
            }
            
            if (rfcTextField.text!.count != 12){
                showToast(message: "El RFC de personas morales debe ser de 12 caracteres")
                return
            }
            
            if (emailTextField.text == ""){
                showToast(message: "Por favor ingresa tu correo")
                return
            }
            
            let isValidateEmail = self.validation.validateEmailId(emailID: emailTextField.text!)
            if isValidateEmail == false {
                showToast(message: "Por favor ingresa un correo válido")
                return
            }
            
            /*
            if (phoneTextField.text == ""){
                showToast(message: "Por favor ingresa tu teléfono")
                return
            }
            
            if (phoneTextField.text?.count != 10){
                showToast(message: "El número de teléfono debe ser de 10 dígitos")
                return
            }
     */
            
            let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phoneTextField.text!)
            if isValidatePhone == false {
                showToast(message: "El número de teléfono debe ser de 10 dígitos numéricos")
                return
            }
            
            
            if (stateTextField.text == ""){
                showToast(message: "Por favor ingresa tu estado")
                return
            }
            
            if (municipioTextField.text == ""){
                showToast(message: "Por favor ingresa tu municipio")
                return
            }
            
            if (direccionTextField.text == ""){
                showToast(message: "Por favor ingresa tu dirección")
                return
            }
            
            /*
            if (cpTextField.text == ""){
                showToast(message: "Por favor ingresa tu código postal")
                return
            }
            
            if (cpTextField.text?.count != 5){
                showToast(message: "El código postal debe ser de 5 dígitos")
                return
            }
     */
            
            let isValidateCP = self.validation.validaCodePostal(code: cpTextField.text!)
            if isValidateCP == false {
                showToast(message: "El código postal debe ser de 5 números")
                return
            }
            
            let docData: [String: Any] = [
                
                "alias": aliasTextField.text!,
                "address": direccionTextField.text!,
                "defaultCdfi": selectedCFDIRow,
                "defaultCompanyId": selectedCompanies!,
                "defaultPaymentMethod": selectedMethodRow,
                "email": emailTextField.text!,
                "enabled": true,
                "id": docId,
                "municipality": municipioTextField.text!,
                "lastNameFather": "",
                "lastNameMother": "",
                "name": RazonSocialMoralTextField.text!,
                "phoneNumber": phoneTextField.text!,
                "postalCode": cpTextField.text!,
                "rfc": rfcTextField.text!,
                "selectedImagePosition": selectedIndex,
                "state": stateTextField.text!,
                "type": selectedTypeRow
                
            ]
            
            db.collection("users").document(userID).collection("profiles").document(docId).setData(docData) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                    print("docData: \(docData)")
                } else {
                    
                    self.sendAddedNewProfileObserver()
                    
                    print("Document successfully written!")
                    print("docData: \(docData)")
                    //let profileData = ProfileDataModel(data: docData)
                    //self.profileArray.append(profileData!)
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                    yourVc.modalPresentationStyle = .fullScreen
                    self.present(yourVc, animated: true, completion: nil)
                    
                    //self.dismiss(animated: true, completion: nil)
                    
                }
            }
        }
        
    
       
        
        print("selectedCompanies: \(selectedCompanies!)")
        
        
        
    }
    
    private func sendAddedNewProfileObserver(){
        NotificationCenter.default.post(name: .addNewProfile, object: nil)
    }
    
    @IBAction func buttonOneTouch(_ sender: UIButton) {
        imageOne.showsTouchWhenHighlighted = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CollectionViewCell
        let myCellImage = UIImage(named: images[indexPath.row])
        myCell.imageCollectionView.image = myCellImage
        return myCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.frame.size.width
        return CGSize(width: cellWidth, height: cellWidth*0.7)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.item
        let data = indexPath.item
        print("data position image: \(data)")
        switch data {
        case 0:
            self.viewPrincipal.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
        case 1:
            self.viewPrincipal.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_city_background")!)
        case 2:
            self.viewPrincipal.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_mountain_background")!)
        case 3:
            self.viewPrincipal.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_pyramid_background")!)
        case 4:
            self.viewPrincipal.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_snow_background")!)
        case 5:
            self.viewPrincipal.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_space_background")!)
            
        default:
            print("Have you done something new?")
        }
        
        collectionView.reloadData()
        
    }
    
}


