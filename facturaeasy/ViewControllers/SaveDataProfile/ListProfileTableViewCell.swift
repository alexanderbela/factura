//
//  ListProfileTableViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 20/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

class ListProfileTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var imageProfileView: UIImageView!
    @IBOutlet weak var nameProfileLabel: UILabel!
    @IBOutlet weak var rfcProfileLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
