//
//  ProfileListDataViewController.swift
//  facturaeasy
//
//  Created by Baxten on 21/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON
import GoogleMobileAds

class ProfileListDataViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, GADBannerViewDelegate, GADInterstitialDelegate  {
    
    @IBOutlet weak var ProfilesDataTableView: UITableView!
    @IBOutlet weak var profileViewWithData: UIView!
    @IBOutlet weak var profileViewEmpty: UIView!
    @IBOutlet weak var imageAddProfile: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var scrollViewMain: UIScrollView!
    @IBOutlet weak var loadingViewProfile: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let imageOn = UIImage(named: "passwordon") as UIImage?
    var db: Firestore!
    var profileArray = [ProfileDataModelInvoices]()
    var listProfileRequester =  ListProfileRequest()
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    var isValid = false
    
    override func viewDidLoad() {
        
        self.activityIndicator.startAnimating()
        self.view.bringSubviewToFront(self.loadingViewProfile)
        db = Firestore.firestore()
        getDataProfilesList()
        ProfilesDataTableView.delegate = self
        ProfilesDataTableView.dataSource = self
        self.ProfilesDataTableView.separatorStyle = .none
        addObservers()
        refreshProfileData()
        addObserversValidatePayment()
        imageAddProfile.image = imageAddProfile.image?.withRenderingMode(.alwaysTemplate)
        imageAddProfile.tintColor = UIColor.white
        
        //let index = alias.firstIndex{$0 === someObject}
        print("entra a viewDidLoad ProfileListDataViewController")
        
        addButton.showsTouchWhenHighlighted = true
        exitButton.showsTouchWhenHighlighted = true
        
        self.validPaymentSingleton()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.validPayment()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9763277647634316/9130453204")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    func loadLittleBanner(){
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-9763277647634316/9869638043"
        //bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //codigo de prueba
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        // Gets the domain from which the error came.
        let errorDomain = error.domain
        // Gets the error code. See
        // https://developers.google.com/admob/ios/api/reference/Enums/GADErrorCode
        // for a list of possible codes.
        let errorCode = error.code
        // Gets an error message.
        // For example "Account not approved yet". See
        // https://support.google.com/admob/answer/9905175 for explanations of
        // common errors.
        let errorMessage = error.localizedDescription
        // Gets additional response information about the request. See
        // https://developers.google.com/admob/ios/response-info for more information.
        let responseInfo = error.userInfo[GADErrorUserInfoKeyResponseInfo] as? GADResponseInfo
        // Gets the underlyingError, if available.
        let underlyingError = error.userInfo[NSUnderlyingErrorKey] as? Error
        if let responseInfo = responseInfo {
            print("Received error with domain: \(errorDomain), code: \(errorCode),"
                    + "message: \(errorMessage), responseInfo: \(responseInfo),"
                    + "underLyingError: \(underlyingError?.localizedDescription ?? "nil")")
        }
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileListDataTableViewCell", for: indexPath) as! ProfileListDataTableViewCell
        let profile = profileArray[indexPath.row]
        
        cell.nameProfileDataLabel.text = profile.alias
        cell.rfcDataProfileLabel.text = profile.rfc
        cell.imageProfileDataView.image = imagePosition(data: profile.imagePosition)
        return cell
    }
    
    func imagePosition(data: Int) -> UIImage{
        
        var imagePos = UIImage(named: "ic_profile_beach_info")!
        
        switch data {
        case 0:
            imagePos = UIImage(named: "ic_profile_beach_info")!
        case 1:
            imagePos = UIImage(named: "ic_profile_city_info")!
        case 2:
            imagePos = UIImage(named: "ic_profile_mountain_info")!
        case 3:
            imagePos = UIImage(named: "ic_profile_pyramid_info")!
        case 4:
            imagePos = UIImage(named: "ic_profile_snow_info")!
        case 5:
            imagePos = UIImage(named: "ic_profile_space_info")!
            
        default:
            imagePos = UIImage(named: "ic_profile_beach_info")!
            
        }
        
        return imagePos;
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let alert = UIAlertController(title: "", message: "¿Estás seguro que deseas eliminar este perfil?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Sí", style: .default, handler: { action in
                let userID = Auth.auth().currentUser!.uid
                let profile = self.profileArray[indexPath.row]
                let id = profile.idProfile
                print("id to delete: \(id)")
                self.db.collection("users").document(userID).collection("profiles").whereField("id", isEqualTo: id)
                    .getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            for document in querySnapshot!.documents {
                                document.reference.delete()
                                print("\(document.documentID) => \(document.data())")
                            }
                        }
                    }
                
                self.profileArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                
                if (self.profileArray.isEmpty == true){
                    print("entra a if para mandar view atrás")
                    self.scrollViewMain.bringSubviewToFront(self.profileViewEmpty)
                    self.scrollViewMain.sendSubviewToBack(self.profileViewWithData)
                    self.view.sendSubviewToBack(self.loadingViewProfile)
                }
                else{
                    self.scrollViewMain.bringSubviewToFront(self.profileViewWithData)
                    self.scrollViewMain.sendSubviewToBack(self.profileViewEmpty)
                    self.view.sendSubviewToBack(self.loadingViewProfile)
                }
                
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler:{ action in
                return
            }))
            self.present(alert, animated: true)
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        
        let profile = profileArray[indexPath.row]
        yourVc.direccionDetail = profile.address
        yourVc.alias = profile.alias
        yourVc.defaultCdfi = profile.defaultCdfi
        yourVc.defaultCompanyId = profile.defaultCompanyId
        yourVc.defaultPaymentMethod = profile.defaultPaymentMethod
        yourVc.email = profile.email
        yourVc.lastNameFather = profile.lastNameFather
        yourVc.lastNameMother = profile.lastNameMother
        yourVc.municipality = profile.municipality
        yourVc.name = profile.name
        yourVc.phoneNumber = profile.phoneNumber
        yourVc.postalCode = profile.postalCode
        yourVc.rfc = profile.rfc
        yourVc.imagePosition = profile.imagePosition
        yourVc.state = profile.state
        yourVc.defaultType = profile.type
        yourVc.idDetail = profile.idProfile
        
        self.present(yourVc, animated: true, completion: nil)
    }
    
    @IBAction func logOutButton(_ sender: UIButton) {
        
        let email = Auth.auth().currentUser?.email
        let alert = UIAlertController(title: "", message: "¿Estás seguro que deseas cerrar la sesión actual de \(email!)?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Sí", style: .default, handler: { action in
            if(self.signOut() == true){
                
                let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                yourVc.modalPresentationStyle = .fullScreen
                self.present(yourVc, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler:{ action in
            return
        }))
        self.present(alert, animated: true)
        
    }
    
    func signOut() -> Bool{
        do{
            try Auth.auth().signOut()
            print("sesión cerrada")
            return true
        }catch{
            print("sesión no cerrada")
            return false
        }
    }
    
    @IBAction func registerProfile(_ sender: UIButton) {
        
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileDataViewController") as! ProfileDataViewController
        //yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
    }
    
    func getDataProfilesList(){
        
        listProfileRequester.getListProfileData(){result in
            switch result {
            case .success(let arrayProfiles):
                self.profileArray = arrayProfiles
                if (self.profileArray.isEmpty == true){
                    self.activityIndicator.stopAnimating()
                    self.view.sendSubviewToBack(self.loadingViewProfile)
                    self.scrollViewMain.bringSubviewToFront(self.profileViewEmpty)
                    self.scrollViewMain.sendSubviewToBack(self.profileViewWithData)
                }
                else{
                    self.activityIndicator.stopAnimating()
                    self.view.sendSubviewToBack(self.loadingViewProfile)
                    self.scrollViewMain.bringSubviewToFront(self.profileViewWithData)
                    self.scrollViewMain.sendSubviewToBack(self.profileViewEmpty)
                }
                
                DispatchQueue.main.async {
                    self.ProfilesDataTableView.reloadData()
                }
            case .failure(_):
                let alert = UIAlertController(title: "", message: "¡Verifique su conexión a internet!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                self.present(alert, animated: true)
                break;
            }
        }
        
    }
    
    @IBAction func registerDataProfile(_ sender: UIButton) {
        
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileDataViewController") as! ProfileDataViewController
        //yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
        
    }
    
    
    func validPaymentSingleton(){
        let isValid = Singleton.shared.getIsValidPayment()?.isValid
        //print("isValid: \(isValid!)")
        let cantidadFacturas = Singleton.shared.getCantidadFacturas()?.cantidad
        //amountFacturas.text = String(10 - cantidadFacturas!)
        let saldo = 10 - cantidadFacturas!
        if saldo != 0 {
            
            self.loadLittleBanner()
            self.addBannerViewToView(self.bannerView)
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
        }else{
            
            let isValid = Singleton.shared.getIsValidPayment()?.isValid
            if isValid! == false{
                self.activityIndicator.stopAnimating()
                self.loadLittleBanner()
                self.addBannerViewToView(self.bannerView)
            }else{
                
                self.activityIndicator.stopAnimating()
                print("Tienes tu mensualidad activa")
                
            }
            
        }
    }
    
    func validPayment(){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        
        let cantidadFacturas = Singleton.shared.getCantidadFacturas()?.cantidad
        //amountFacturas.text = String(10 - cantidadFacturas!)
        let saldo = 10 - cantidadFacturas!
        if saldo != 0 {
            
            
            self.loadLittleBanner()
            self.addBannerViewToView(self.bannerView)
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
        }else{
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
            
            let docRef = db.collection("validateMonthlyPaymentByUser").document(userID)
            
            docRef.getDocument { (document, error) in
                
                if let document = document, document.exists {
                    
                    
                    let serverTimestamp = document.get("timeStampEnd") as! String
                    let date = NSDate(timeIntervalSince1970: Double(serverTimestamp)!)
                    let dateTod = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"
                    
                    let dateToday2 = formatter.string(from: dateTod)
                    let dateServer2 = formatter.string(from: date as Date)
                    
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                    dateFormatter2.dateFormat = "dd-MM-yyyy"
                    let dateToday = dateFormatter2.date(from:dateToday2)
                    let dateServer = dateFormatter2.date(from:dateServer2)
                    
                    print("dateToday: \(dateToday!)")
                    print("dateServer: \(dateServer!)")
                    
                    if(dateServer! <= dateToday!){
                        
                        
                        self.isValid = false
                        print("Tu suscripción ha vencido")
                        
                        let isVal = isValidPayment(isValid: self.isValid)
                        Singleton.shared.setIsValidPayment(name: isVal)
                        
                        self.loadLittleBanner()
                        self.addBannerViewToView(self.bannerView)
                        
                    }else{
                        
                        self.isValid = true
                        print("Tu suscripción sigue vigente")
                        let isVal = isValidPayment(isValid: self.isValid)
                        Singleton.shared.setIsValidPayment(name: isVal)
                        //self.validatePaymentObserver(isValidPayment: isValid)
                        
                        
                    }
                    
                    
                    
                } else {
                    
                    //isValid = false
                    print("Tu suscripción ha vencido")
                    
                    
                    print("Document does not exist")
                    
                    self.loadLittleBanner()
                    self.addBannerViewToView(self.bannerView)
                }
            }
        }
        
    }
    
}


extension ProfileListDataViewController {
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadUsersNotification), name: .addNewProfile, object: nil)
    }
    
    @objc private func reloadUsersNotification(notification:Notification){
        
        print("ProfileListDavaViewController")
        
        self.profileArray.removeAll()
        getDataProfilesList()
    }
    
}

extension ProfileListDataViewController {
    
    func refreshProfileData() {
        NotificationCenter.default.addObserver(self, selector: #selector(profileRefresh), name: .profileRefresh, object: nil)
    }
    
    @objc func profileRefresh(_ notification: Notification) {
        
        print("ProfileListDavaViewController")
        self.profileArray.removeAll()
        getDataProfilesList()
        
    }
    
    func addObserversValidatePayment() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceivedValidationPayment), name: .validPayment, object: nil)
    }
    
    @objc func notificationReceivedValidationPayment(_ notification: Notification) {
        
        self.validPaymentSingleton()
        
    }
    
}

