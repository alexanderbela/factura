//
//  ListProfilesViewController.swift
//  facturaeasy
//
//  Created by Baxten on 20/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

class ListProfilesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    
    @IBOutlet weak var ProfilesTableView: UITableView!
    
    var name = [String]()
    
    
    var allTextFromImage = ""
    
    
    var direccion = [String]()
    var alias = [String]()
    var defaultCdfi = [Int]()
    var defaultCompanyId = [String]()
    var defaultPaymentMethod = [Int]()
    var email = [String]()
    var idProfile = [String]()
    
    var lastNameFather = [String]()
    var lastNameMother = [String]()
    var municipality = [String]()
    
    var phoneNumber = [String]()
    var postalCode = [String]()
    var state = [String]()
    var type = [Int]()
    var imagePosDetail = [Int]()
    
    var nameProfile = [String]()
    var rfc = [String]()
    var imagePos = [Int]()
    
    let imageOn = UIImage(named: "passwordon") as UIImage?
    var db: Firestore!
    var listProfileRequester = ListProfileRequest()
    var arrayListProfile = [ProfileDataModelInvoices]()
    
    override func viewDidLoad() {
        
        db = Firestore.firestore()
        ProfilesTableView.delegate = self
        ProfilesTableView.dataSource = self
        
        getDataProfilesList()
        view?.backgroundColor = UIColor(white: 0.1, alpha: 0.6)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("entra a viewWillAppear ListProfilesViewController");
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayListProfile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListProfileTableViewCell", for: indexPath) as! ListProfileTableViewCell
        let elements = self.arrayListProfile[indexPath.row]
        
        cell.nameProfileLabel.text = elements.alias
        cell.rfcProfileLabel.text = elements.rfc
        cell.imageProfileView.image = imagePosition(data: elements.imagePosition)
        return cell
    }
    
    func imagePosition(data: Int) -> UIImage{
        
        var imagePos = UIImage(named: "ic_profile_beach_info")!
        
        switch data {
        case 0:
            imagePos = UIImage(named: "ic_profile_beach_info")!
        case 1:
            imagePos = UIImage(named: "ic_profile_city_info")!
        case 2:
            imagePos = UIImage(named: "ic_profile_mountain_info")!
        case 3:
            imagePos = UIImage(named: "ic_profile_pyramid_info")!
        case 4:
            imagePos = UIImage(named: "ic_profile_snow_info")!
        case 5:
            imagePos = UIImage(named: "ic_profile_space_info")!
            
        default:
            imagePos = UIImage(named: "ic_profile_beach_info")!
            
        }
        
        return imagePos;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProcessTicketViewController") as! ProcessTicketViewController
        
        let elements = arrayListProfile[indexPath.row]
        yourVc.getDataProfile = elements
        /*
        yourVc.imageProcess = elements.imagePosition
        yourVc.direccion = elements.address
        yourVc.aliasProfile = elements.alias
        yourVc.cfdiProfile = elements.defaultCdfi
        yourVc.companyProfile = elements.defaultCompanyId
        yourVc.paymentMethod = elements.defaultPaymentMethod
        yourVc.email = elements.email
        yourVc.idProfile = elements.idProfile
        yourVc.lastNameFather = elements.lastNameFather
        yourVc.lastNameMother = elements.lastNameMother
        yourVc.municipality = elements.municipality
        yourVc.nameProfile = elements.name
        yourVc.phoneNumber = elements.phoneNumber
        yourVc.postalCode = elements.postalCode
        yourVc.rfc = elements.rfc
        yourVc.imagePos = elements.imagePosition
        yourVc.state = elements.state
        yourVc.type = elements.type
 */
        
        yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
    }
    
    func getDataProfilesList(){
        
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        self.showSpinner(onView: self.view)
        listProfileRequester.getListProfileData(){ result in
            self.removeSpinner()
            switch result {
            case .success(let arrayProfile):
                self.arrayListProfile = arrayProfile
                self.ProfilesTableView.reloadData()
            case .failure(let error):
                break;
            }
        }
            
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
     
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
}
