//
//  CollectionViewDetailCollectionViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 20/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

class CollectionViewDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageCollectionViewDetail: UIImageView!
    
}
