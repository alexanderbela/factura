//
//  ProfileDetailsViewController.swift
//  facturaeasy
//
//  Created by Baxten on 04/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

class ProfileDetailsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    @IBOutlet weak var imageBackgroundProfileView: UIView!
    @IBOutlet var viewPrincipalImage: UIView!
    
    
    @IBOutlet weak var aliasDetailTextField: UITextField!
    @IBOutlet weak var typeDatailPickerTextField: UITextField!
    @IBOutlet weak var nameDetailTextField: UITextField!
    @IBOutlet weak var lastNameDetailTextField: UITextField!
    @IBOutlet weak var secondLastNameDetailTextField: UITextField!
    @IBOutlet weak var rfcDetailTextField: UITextField!
    @IBOutlet weak var emailDetailTextField: UITextField!
    @IBOutlet weak var phoneNumberDetailTextField: UITextField!
    @IBOutlet weak var stateDetailTextField: UITextField!
    @IBOutlet weak var municipalyDetailTextField: UITextField!
    @IBOutlet weak var directionDetailTextField: UITextField!
    @IBOutlet weak var cpDetailTextField: UITextField!
    @IBOutlet weak var useCFDIDetailPickerTextField: UITextField!
    @IBOutlet weak var paymentMethodDetailPickerTextField: UITextField!
    @IBOutlet weak var negocioDetailTextField: UITextField!
    @IBOutlet weak var razonSocialTextField: UITextField!
    @IBOutlet weak var collectionViewDetail: UICollectionView!
    @IBOutlet weak var viewWithTypes: UIView!
    @IBOutlet weak var viewPersonaFisica: UIView!
    @IBOutlet weak var viewPersonaMoral: UIView!
    
    
    var idProfile: String = ""
    var direccionDetail: String = ""
    var db: Firestore!
    
    var direccion: String = ""
    var alias: String = ""
    var defaultCdfi: Int = 0
    var defaultCompanyId: String = ""
    var defaultPaymentMethod: Int = 0
    var email: String = ""
    var lastNameFather: String = ""
    var lastNameMother: String = ""
    var municipality: String = ""
    var name: String = ""
    var phoneNumber: String = ""
    var postalCode: String = ""
    var rfc: String = ""
    var imagePosition: Int = 0
    var state: String = ""
    var defaultType: Int = 0
    var idDetail: String = ""
    
    var positionCompanies = ""
    
    
    var pickerViewType = UIPickerView()
    var pickerViewMethods = UIPickerView()
    var pickerViewCFDI = UIPickerView()
    var pickerViewCompanies = UIPickerView()
    
    var selectedTypeDetail: String?
    var selectedTypeMethodDetail: String?
    var selectedCFDIDetail: String?
    var selectedCompaniesDetail: String?
    
    var selectedTypeRow: Int = 0
    var selectedMethodRow: Int = 0
    var selectedCFDIRow: Int = 0
    
    var rowSelected: String = ""
    var typesCount: Int = 0;
    
    var selectedIndex: Int = 0
    
    var images: [String] = [
        "ic_profile_beach_button", "ic_profile_city_button", "ic_profile_mountain_button",
        "ic_profile_pyramid_button", "ic_profile_snow_button", "ic_profile_space_button"
    ]
    
    var typesCompaniesDetail = Singleton.shared.getCompanies()?.companies
    var typesMethodsDetail = Singleton.shared.getPayment()?.payment
    var typesDetail = Singleton.shared.getTypes()?.types
    var typesCFDIDetail = Singleton.shared.getCFDI()?.cfdi
    var validation = Validation()
    
    override func viewDidLoad() {
        
        db = Firestore.firestore()
        
        //getDataProfile()
        
        //phoneNumberDetailTextField.keyboardType = .numberPad
        //cpDetailTextField.keyboardType = .numberPad
        
        getDataCompaniesName()
        getDataTypes(position: defaultType)
        getDataMethods(position: defaultPaymentMethod)
        getDataCFDI(position: defaultCdfi)
        
        createPickerView()
        dismissPickerView()
        
        //self.imagePositionImage(data: imagePosition)
        self.backgroundImageView(data: imagePosition)
        self.selectedIndex = self.imagePosition
        self.selectedCFDIRow = self.defaultCdfi
        self.selectedMethodRow = self.defaultPaymentMethod
        self.selectedTypeRow = self.defaultType
        print("entra a ProfileDetailsViewController ")
        
        self.dismissKey()
        iconsTextField()
        getData()
        
        
    }
    
    
    
    func iconsTextField(){
        
        typeDatailPickerTextField.rightViewMode = UITextField.ViewMode.always
        typeDatailPickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        paymentMethodDetailPickerTextField.rightViewMode = UITextField.ViewMode.always
        paymentMethodDetailPickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        useCFDIDetailPickerTextField.rightViewMode = UITextField.ViewMode.always
        useCFDIDetailPickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        negocioDetailTextField.rightViewMode = UITextField.ViewMode.always
        negocioDetailTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
    }
    
    func createPickerView() {
        
        pickerViewType.delegate = self
        typeDatailPickerTextField.inputView = pickerViewType
        
        pickerViewMethods.delegate = self
        paymentMethodDetailPickerTextField.inputView = pickerViewMethods
        
        pickerViewCFDI.delegate = self
        useCFDIDetailPickerTextField.inputView = pickerViewCFDI
        
        pickerViewCompanies.delegate = self
        negocioDetailTextField.inputView = pickerViewCompanies
        
    }
    
    func dismissPickerView() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        typeDatailPickerTextField.inputAccessoryView = toolBar
        
        let toolBarMethod = UIToolbar()
        toolBarMethod.sizeToFit()
        let buttonMethod = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarMethod.setItems([buttonMethod], animated: true)
        toolBarMethod.isUserInteractionEnabled = true
        paymentMethodDetailPickerTextField.inputAccessoryView = toolBarMethod
        
        let toolBarCFDI = UIToolbar()
        toolBarCFDI.sizeToFit()
        let buttonCFDI = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarCFDI.setItems([buttonCFDI], animated: true)
        toolBarCFDI.isUserInteractionEnabled = true
        useCFDIDetailPickerTextField.inputAccessoryView = toolBarCFDI
        
        let toolBarCompanies = UIToolbar()
        toolBarCompanies.sizeToFit()
        let buttonCompanies = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarCompanies.setItems([buttonCompanies], animated: true)
        toolBarCompanies.isUserInteractionEnabled = true
        negocioDetailTextField.inputAccessoryView = toolBarCompanies
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.pickerViewType {
            typesCount = typesDetail!.count
        }
        
        if pickerView == self.pickerViewMethods {
            typesCount = typesMethodsDetail!.count
        }
        
        if pickerView == self.pickerViewCFDI {
            typesCount = typesCFDIDetail!.count
        }
        
        if pickerView == self.pickerViewCompanies {
            typesCount = typesCompaniesDetail!.count
        }
        
        return typesCount
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.pickerViewType {
            rowSelected = typesDetail![row]
        }
        
        if pickerView == self.pickerViewMethods {
            rowSelected = typesMethodsDetail![row]
        }
        
        if pickerView == self.pickerViewCFDI {
            rowSelected = typesCFDIDetail![row]
        }
        
        if pickerView == self.pickerViewCompanies {
            rowSelected = typesCompaniesDetail![row]
        }
        
        return rowSelected
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if pickerView == self.pickerViewType {
            selectedTypeRow = row
            selectedTypeDetail = typesDetail![row] // selected item
            typeDatailPickerTextField.text = selectedTypeDetail
            print("selectedTypeRow: \(selectedTypeRow)")
            print("selectedType: \(selectedTypeDetail!)")
            
            if self.selectedTypeDetail == "Persona física"{
                self.viewWithTypes.bringSubviewToFront(self.viewPersonaFisica)
                self.viewWithTypes.sendSubviewToBack(self.viewPersonaMoral)
            }else{
                self.viewWithTypes.bringSubviewToFront(self.viewPersonaMoral)
                self.viewWithTypes.sendSubviewToBack(self.viewPersonaFisica)
            }
            
            
           
        }
        
        if pickerView == pickerViewMethods {
            selectedMethodRow = row
            selectedTypeMethodDetail = typesMethodsDetail![row] // selected item
            paymentMethodDetailPickerTextField.text = selectedTypeMethodDetail
            print("selectedMethodRow: \(selectedMethodRow)")
            print("selectedType: \(selectedTypeMethodDetail!)")
        }
        
        if pickerView == pickerViewCFDI {
            selectedCFDIRow = row
            selectedCFDIDetail = typesCFDIDetail![row] // selected item
            useCFDIDetailPickerTextField.text = selectedCFDIDetail
            print("selectedType: \(selectedCFDIRow)")
            print("selectedType: \(selectedCFDIDetail!)")
        }
        
        if pickerView == pickerViewCompanies {
            selectedCompaniesDetail = typesCompaniesDetail![row] // selected item
            negocioDetailTextField.text = selectedCompaniesDetail
            print("selectedType: \(selectedCompaniesDetail!)")
        }
        
    }
    
    
    func getData(){
        print("---------------- datos recibidos -------------------------")
        print("direccionDetail: \(direccionDetail)")
        print("alias: \(alias)")
        print("defaultCdfi: \(defaultCdfi)")
        print("defaultCompanyId: \(defaultCompanyId)")
        print("defaultPaymentMethod: \(defaultPaymentMethod)")
        print("email: \(email)")
        print("lastNameFather: \(lastNameFather)")
        print("lastNameMother: \(lastNameMother)")
        print("municipality: \(municipality)")
        print("name: \(name)")
        print("phoneNumber: \(phoneNumber)")
        print("postalCode: \(postalCode)")
        print("rfc: \(rfc)")
        print("imagePosition: \(imagePosition)")
        print("state: \(state)")
        print("type: \(defaultType)")
        print("id: \(idDetail)")
        print("-----------------------------------------")
        
        //let indexOfA = self.typesDetail!.firstIndex(of: String(self.defaultType))
        self.pickerViewType.selectRow(self.defaultType, inComponent: 0, animated: true)
        self.pickerViewCFDI.selectRow(self.defaultCdfi, inComponent: 0, animated: true)
        self.pickerViewMethods.selectRow(self.defaultPaymentMethod, inComponent: 0, animated: true)
        
        //let index = self.typesCompaniesDetail!.firstIndex(of: self.defaultCompanyId)
        let indexOfA = self.typesCompaniesDetail!.firstIndex(of: self.defaultCompanyId)!
        self.pickerViewCompanies.selectRow(indexOfA, inComponent: 0, animated: true)
        
        directionDetailTextField.text = direccionDetail
        aliasDetailTextField.text = alias
        //useCFDIDetailPickerTextField.text = defaultCdfi
        negocioDetailTextField.text = defaultCompanyId
        emailDetailTextField.text = email
        lastNameDetailTextField.text = lastNameFather
        secondLastNameDetailTextField.text = lastNameMother
        municipalyDetailTextField.text = municipality
        nameDetailTextField.text = name
        razonSocialTextField.text = name
        phoneNumberDetailTextField.text = phoneNumber
        cpDetailTextField.text = postalCode
        rfcDetailTextField.text = rfc
        stateDetailTextField.text = state
        
        
    }
    
    
    func backgroundImageView(data: Int){
        //let data = indexPath.item
        print("data position image: \(data)")
        switch data {
        case 0:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
        case 1:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_city_background")!)
        case 2:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_mountain_background")!)
        case 3:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_pyramid_background")!)
        case 4:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_snow_background")!)
        case 5:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_space_background")!)
            
        default:
            print("Have you done something new?")
        }
    }
    
    
    func imagePositionImage(data: Int) -> UIImage{
        
        var imagePos = UIImage(named: "ic_profile_beach_info")!
        switch data {
        case 0:
            imagePos = UIImage(named: "ic_profile_beach_info")!
        case 1:
            imagePos = UIImage(named: "ic_profile_city_info")!
        case 2:
            imagePos = UIImage(named: "ic_profile_mountain_info")!
        case 3:
            imagePos = UIImage(named: "ic_profile_pyramid_info")!
        case 4:
            imagePos = UIImage(named: "ic_profile_snow_info")!
        case 5:
            imagePos = UIImage(named: "ic_profile_space_info")!
        default:
            print("Have you done something new?")
            
        }
        
        return imagePos;
        
    }
    
    
    func getDataProfile(){
        
        
        /*
         let userID = Auth.auth().currentUser!.uid
         print("userID: \(userID)")
         
         self.showSpinner(onView: self.view)
         self.db.collection("users").document(userID).collection("profiles").whereField("id", isEqualTo: idProfile)
         .getDocuments() { (querySnapshot, err) in
         
         if let err = err {
         print("Error getting documents: \(err)")
         } else {
         for document in snapshot!.documents {
         
         let docId = document.documentID
         let alias = document.get("name") as! String
         let rfc = document.get("rfc") as! String
         let imagePosition = document.get("selectedImagePosition") as! Int
         
         self.name.append(alias)
         self.rfc.append(rfc)
         self.imagePos.append(imagePosition)
         self.idProfile.append(docId)
         
         print(docId, alias, rfc)
         }
         
         
         self.ProfilesDataTableView.reloadData()
         print("names: \(self.name)")
         print("rfc: \(self.rfc)")
         print("imagePos: \(self.imagePos)")
         }
         }
         */
    }
    
    func getDataCompaniesName(){
        
        //self.negocioDetailTextField.text = self.typesCompaniesDetail.first
        self.selectedCompaniesDetail = self.typesCompaniesDetail?.first
        //print("selectedCompanies: \(self.selectedCompanies!)")
        //print(self.typesCompanies)
        
        let index = self.typesCompaniesDetail!.firstIndex(of: self.defaultCompanyId)
        print("Found peaches at index \(index!)")
        
        
        self.negocioDetailTextField.text = self.typesCompaniesDetail![index!]
        self.pickerViewCompanies.selectRow(index!, inComponent: 0, animated: true)
        self.selectedCompaniesDetail = self.typesCompaniesDetail![index!]
        
        
    }
    
    func getDataMethods(position: Int){
        
        self.paymentMethodDetailPickerTextField.text = self.typesMethodsDetail![position]
        self.pickerViewMethods.selectRow(position, inComponent: 0, animated: true)
        self.selectedTypeMethodDetail = self.typesMethodsDetail![position]
        print(self.typesMethodsDetail![position])
       
    }
    
    func getDataCFDI(position: Int){
        
        self.useCFDIDetailPickerTextField.text = self.typesCFDIDetail![position]
        self.pickerViewCFDI.selectRow(position, inComponent: 0, animated: true)
        self.selectedCFDIDetail = self.typesCFDIDetail![position]
        print(self.typesCFDIDetail![position])
        
    }
    
    func getDataTypes(position: Int){
        print("position type: \(position)")
        self.typeDatailPickerTextField.text = self.typesDetail![position]
        self.pickerViewType.selectRow(position, inComponent: 0, animated: true)
        self.selectedTypeDetail = self.typesDetail![position]
        print("self.typesDetail![position]: \(self.typesDetail![position])")
        
        if self.selectedTypeDetail == "Persona física"{
            self.viewWithTypes.bringSubviewToFront(self.viewPersonaFisica)
            self.viewWithTypes.sendSubviewToBack(self.viewPersonaMoral)
        }else{
            self.viewWithTypes.bringSubviewToFront(self.viewPersonaMoral)
            self.viewWithTypes.sendSubviewToBack(self.viewPersonaFisica)
        }
        
    }
    
    
    @IBAction func updateData(_ sender: UIButton) {
        
        let userID = Auth.auth().currentUser!.uid
        
        let ref = db.collection("users")
        let docId = self.idDetail
        
        if self.selectedTypeDetail == "Persona física"{
            if (aliasDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu alias")
                return
            }
            
            if (nameDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu nombre")
                return
            }
            
            if (lastNameDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu apellido paterno")
                return
            }
            
            if (secondLastNameDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu apellido materno")
                return
            }
            
            if (rfcDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu RFC")
                return
            }
            
            if (rfcDetailTextField.text!.count <= 12 || rfcDetailTextField.text!.count >= 14){
                showToast(message: "El RFC de personas físicas debe ser de 13 caracteres")
                return
            }
            
            if (emailDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu correo")
                return
            }
            
            let isValidateEmail = self.validation.validateEmailId(emailID: emailDetailTextField.text!)
            if isValidateEmail == false {
                showToast(message: "Por favor ingresa un correo válido")
                return
            }
            
            
            let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phoneNumberDetailTextField.text!)
            if isValidatePhone == false {
                showToast(message: "El número de teléfono debe ser de 10 dígitos numéricos")
                return
            }
            
            
            if (stateDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu estado")
                return
            }
            
            if (municipalyDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu municipio")
                return
            }
            
            if (directionDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu dirección")
                return
            }
            
            
            let isValidateCP = self.validation.validaCodePostal(code: cpDetailTextField.text!)
            if isValidateCP == false {
                showToast(message: "El código postal debe ser de 5 números")
                return
            }
            
            
            
            let alias = aliasDetailTextField.text
            var type = self.defaultType
            let name = nameDetailTextField.text
            let lastName = lastNameDetailTextField.text
            let secondName = secondLastNameDetailTextField.text
            let rfc = rfcDetailTextField.text
            let email = emailDetailTextField.text
            let tel = phoneNumberDetailTextField.text
            let state = stateDetailTextField.text
            let municipio = municipalyDetailTextField.text
            let direccion = directionDetailTextField.text
            let cp = cpDetailTextField.text
            var cfdi = self.defaultCdfi
            var paymentMethod = self.defaultPaymentMethod
            let negocio = self.selectedCompaniesDetail
            var positionImage = self.imagePosition
            
            if(self.selectedIndex != self.imagePosition){
                positionImage = self.selectedIndex
            }
            else{
                positionImage = self.imagePosition
            }
            
            if(self.selectedCFDIRow != self.defaultCdfi){
                cfdi = self.selectedCFDIRow
            }
            else{
                cfdi = self.defaultCdfi
            }
            
            if(self.selectedMethodRow != self.defaultPaymentMethod){
                paymentMethod = self.selectedMethodRow
            }
            else{
                paymentMethod = self.defaultPaymentMethod
            }
            
            if(self.selectedTypeRow != self.defaultType){
                type = self.selectedTypeRow
            }
            else{
                type = self.defaultType
            }
            
            
            
            print("-------------------------------------------")
            print("Datos nuevos...")
            print("alias: \(alias!)")
            print("type: \(type)")
            print("name: \(name!)")
            print("lastName: \(lastName!)")
            print("secondName: \(secondName!)")
            print("rfc: \(rfc!)")
            print("email: \(email!)")
            print("tel: \(tel!)")
            print("state: \(state!)")
            print("municipio: \(municipio!)")
            print("direccion: \(direccion!)")
            print("cp: \(cp!)")
            print("cfdi: \(cfdi)")
            print("paymentMethod: \(paymentMethod)")
            print("negocio: \(negocio!)")
            print("positionImage: \(positionImage)")
            print("idDetail: \(docId)")
            print("-------------------------------------------")
            
            let docData: [String: Any] = [
                
                "alias": alias!,
                "address": direccion!,
                "defaultCdfi": cfdi,
                "defaultCompanyId": negocio!,
                "defaultPaymentMethod": paymentMethod,
                "email": email!,
                "enabled": true,
                "id": docId,
                "lastNameFather": lastName!,
                "lastNameMother": secondName!,
                "municipality": municipio!,
                "name": name!,
                "phoneNumber": tel!,
                "postalCode": cp!,
                "rfc": rfc!,
                "selectedImagePosition": positionImage,
                "state": state!,
                "type": type
                
            ]
            
            db.collection("users").document(userID).collection("profiles").document(docId).setData(docData) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                    print("docData: \(docData)")
                } else {
                    
                    self.sendAddedNewProfileObserver()
                    
                    print("Document successfully written!")
                    print("docData: \(docData)")
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }
        }else{
            
            if (aliasDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu alias")
                return
            }
            
            if (nameDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu nombre")
                return
            }
            
            
            if (rfcDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu RFC")
                return
            }
            
            if (rfcDetailTextField.text!.count != 12){
                showToast(message: "El RFC de personas morales debe ser de 12 caracteres")
                return
            }
            
            if (emailDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu correo")
                return
            }
            
            let isValidateEmail = self.validation.validateEmailId(emailID: emailDetailTextField.text!)
            if isValidateEmail == false {
                showToast(message: "Por favor ingresa un correo válido")
                return
            }
            
            
            let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phoneNumberDetailTextField.text!)
            if isValidatePhone == false {
                showToast(message: "El número de teléfono debe ser de 10 dígitos numéricos")
                return
            }
            
            
            if (stateDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu estado")
                return
            }
            
            if (municipalyDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu municipio")
                return
            }
            
            if (directionDetailTextField.text == ""){
                showToast(message: "Por favor ingresa tu dirección")
                return
            }
            
            
            let isValidateCP = self.validation.validaCodePostal(code: cpDetailTextField.text!)
            if isValidateCP == false {
                showToast(message: "El código postal debe ser de 5 números")
                return
            }
            
            
            
            let alias = aliasDetailTextField.text
            var type = self.defaultType
            let name = nameDetailTextField.text
            let rfc = rfcDetailTextField.text
            let email = emailDetailTextField.text
            let tel = phoneNumberDetailTextField.text
            let state = stateDetailTextField.text
            let municipio = municipalyDetailTextField.text
            let direccion = directionDetailTextField.text
            let cp = cpDetailTextField.text
            var cfdi = self.defaultCdfi
            var paymentMethod = self.defaultPaymentMethod
            let negocio = self.selectedCompaniesDetail
            var positionImage = self.imagePosition
            let razonSocial = razonSocialTextField.text
            
            if(self.selectedIndex != self.imagePosition){
                positionImage = self.selectedIndex
            }
            else{
                positionImage = self.imagePosition
            }
            
            if(self.selectedCFDIRow != self.defaultCdfi){
                cfdi = self.selectedCFDIRow
            }
            else{
                cfdi = self.defaultCdfi
            }
            
            if(self.selectedMethodRow != self.defaultPaymentMethod){
                paymentMethod = self.selectedMethodRow
            }
            else{
                paymentMethod = self.defaultPaymentMethod
            }
            
            if(self.selectedTypeRow != self.defaultType){
                type = self.selectedTypeRow
            }
            else{
                type = self.defaultType
            }
            
            
            
            print("-------------------------------------------")
            print("Datos nuevos...")
            print("alias: \(alias!)")
            print("type: \(type)")
            print("name: \(name!)")
            print("razonSocial: \(razonSocial!)")
            print("rfc: \(rfc!)")
            print("email: \(email!)")
            print("tel: \(tel!)")
            print("state: \(state!)")
            print("municipio: \(municipio!)")
            print("direccion: \(direccion!)")
            print("cp: \(cp!)")
            print("cfdi: \(cfdi)")
            print("paymentMethod: \(paymentMethod)")
            print("negocio: \(negocio!)")
            print("positionImage: \(positionImage)")
            print("idDetail: \(docId)")
            print("-------------------------------------------")
            
            let docData: [String: Any] = [
                
                "alias": alias!,
                "address": direccion!,
                "defaultCdfi": cfdi,
                "defaultCompanyId": negocio!,
                "defaultPaymentMethod": paymentMethod,
                "email": email!,
                "enabled": true,
                "id": docId,
                "lastNameFather": "",
                "lastNameMother": "",
                "municipality": municipio!,
                "name": razonSocial!,
                "phoneNumber": tel!,
                "postalCode": cp!,
                "rfc": rfc!,
                "selectedImagePosition": positionImage,
                "state": state!,
                "type": type
                
            ]
            
            db.collection("users").document(userID).collection("profiles").document(docId).setData(docData) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                    print("docData: \(docData)")
                } else {
                    
                    self.sendAddedNewProfileObserver()
                    
                    print("Document successfully written!")
                    print("docData: \(docData)")
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }
        }
        
        
        
        
        
        
    }
    
    private func sendAddedNewProfileObserver(){
        NotificationCenter.default.post(name: .addNewProfile, object: nil)
    }
    
}

extension ProfileDetailsViewController {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CollectionViewDetailCollectionViewCell
        let myCellImage = UIImage(named: images[indexPath.row])
        myCell.imageCollectionViewDetail.image = myCellImage
        return myCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.frame.size.width
        return CGSize(width: cellWidth, height: cellWidth*0.7)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.item
        let data = indexPath.item
        print("data position image: \(data)")
        switch data {
        case 0:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
        case 1:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_city_background")!)
        case 2:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_mountain_background")!)
        case 3:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_pyramid_background")!)
        case 4:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_snow_background")!)
        case 5:
            self.viewPrincipalImage.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_space_background")!)
            
        default:
            print("Have you done something new?")
        }
        
        collectionView.reloadData()
        
    }
    
}
