//
//  ProfileListDataTableViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 21/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

class ProfileListDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageProfileDataView: UIImageView!
    @IBOutlet weak var nameProfileDataLabel: UILabel!
    @IBOutlet weak var rfcDataProfileLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
