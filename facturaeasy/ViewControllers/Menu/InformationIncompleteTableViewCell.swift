//
//  InformationIncompleteTableViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 26/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

class InformationIncompleteTableViewCell: UITableViewCell {

    @IBOutlet weak var nameKeyIncompleteInformationLabel: UILabel!
    @IBOutlet weak var valueIncompleteInformationTextField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(lblValue : String, txtValue :String){

        nameKeyIncompleteInformationLabel.text = lblValue
        valueIncompleteInformationTextField.text = txtValue
    }

}
