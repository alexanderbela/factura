//
//  InformationRequiredTableViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 12/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

class InformationRequiredTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameKeyInformation: UILabel!
    @IBOutlet weak var valueInformationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
