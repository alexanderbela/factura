//
//  FilterDataViewController.swift
//  facturaeasy
//
//  Created by Baxten on 23/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON
import GoogleMobileAds


extension Notification.Name {
    static let addFilterFactura = Notification.Name(
        rawValue: "filterFacturaAdded")
    
}

@available(iOS 13.4, *)
class FilterDataViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var statusFacturaTextField: UITextField!
    @IBOutlet weak var profileTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var dateInicialTextField: UITextField!
    @IBOutlet weak var dateFinalTextField: UITextField!
    
    let inicialDatePicker = UIDatePicker()
    let finalDatePicker = UIDatePicker()
    var statusPickerView = UIPickerView()
    var profilePickerView = UIPickerView()
    var CompanyPickerView = UIPickerView()
    var typesCompanies = [String]()
    var typesProfiles = [String]()
    var typesStatusFactura = ["Selecciona...","COMPLETED", "INCOMPLETE", "PENDING", "FAILED"]
    var rowSelected: String = ""
    var selectedCompanies: Int = 0
    var selectedProfiles: Int = 0
    var selectedStatusFactura: Int = 0
    var selectedTypeCompanies: String = ""
    var selectedTypeProfiles: String = ""
    var selectedTypeStatusFactura: String = ""
    var typesCount: Int = 0;
    var db: Firestore!
    
    
    override func viewDidLoad() {
        db = Firestore.firestore()
        getDataCompaniesNameFilter()
        getDataProfilesListFilter()
        createPickerView()
        dismissPickerView()
        self.dismissKey()
        self.statusFacturaTextField.text = self.typesStatusFactura.first
        self.selectedTypeStatusFactura = self.typesStatusFactura.first!
        //self.typesStatusFactura
        showDatePicker()
        startDateOfMonth()
        currentDate()
        iconsTextField()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.statusPickerView {
            typesCount = typesStatusFactura.count
        }
        
        if pickerView == self.profilePickerView {
            typesCount = typesProfiles.count
        }
        
        if pickerView == self.CompanyPickerView {
            typesCount = typesCompanies.count
        }
        
        return typesCount
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.statusPickerView {
            rowSelected = typesStatusFactura[row]
        }
        
        if pickerView == self.profilePickerView {
            rowSelected = typesProfiles[row]
        }
        
        
        if pickerView == self.CompanyPickerView {
            rowSelected = typesCompanies[row]
        }
        
        return rowSelected
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.statusPickerView {
            selectedStatusFactura = row
            selectedTypeStatusFactura = typesStatusFactura[row] // selected item
            statusFacturaTextField.text = selectedTypeStatusFactura
            print("selectedTypeStatusFactura: \(selectedTypeStatusFactura)")
        }
        
        if pickerView == profilePickerView {
            selectedProfiles = row
            selectedTypeProfiles = typesProfiles[row] // selected item
            profileTextField.text = selectedTypeProfiles
            print("selectedTypeProfiles: \(selectedTypeProfiles)")
        }
        
        if pickerView == CompanyPickerView {
            selectedCompanies = row
            selectedTypeCompanies = typesCompanies[row] // selected item
            companyTextField.text = selectedTypeCompanies
            print("selectedCompanies: \(selectedTypeCompanies)")
        }
        
    }
    
    @IBAction func appliedFilterButton(_ sender: UIButton) {
        
        let dateInicial = dateInicialTextField.text!
        let dateFinal = dateFinalTextField.text!
        let statusFactura = selectedTypeStatusFactura
        let profile = selectedTypeProfiles
        let companies = selectedTypeCompanies
        print("fecha inicial seleccionada: \(dateInicial)")
        print("fecha final seleccionada: \(dateFinal)")
        print("statusFactura: \(statusFactura)")
        print("profile!: \(profile)")
        print("companies: \(companies)")
        
        self.sendAddedFilterFactura(dateInicial: dateInicial, dateFinal: dateFinal, statusFactura: statusFactura, profile: profile, companies: companies)
        
    }
    
    private func sendAddedFilterFactura(dateInicial: String, dateFinal: String, statusFactura: String , profile: String, companies: String){
        let userInfo = ["dateInicial": dateInicial, "dateFinal": dateFinal, "statusFactura": statusFactura, "profile": profile ,"companies": companies ]
        NotificationCenter.default.post(name: .addFilterFactura, object: nil, userInfo: userInfo)
        self.dismiss(animated: true, completion: nil)
    }
    
}

@available(iOS 13.4, *)
extension FilterDataViewController {
    
    func getDataProfilesListFilter(){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        
        self.showSpinner(onView: self.view)
        self.db.collection("users").document(userID).collection("profiles").getDocuments { (snapshot, err) in
            
            self.removeSpinner()
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                let selec = "Selecciona..."
                self.typesProfiles.append(selec)
                for document in snapshot!.documents {
                    let alias = document.get("alias") as! String
                    self.typesProfiles.append(alias)
                }
                self.profileTextField.text = self.typesProfiles.first
                self.selectedTypeProfiles = self.typesProfiles.first!
                
                
            }
        }
        
    }
    
    
    func getDataCompaniesNameFilter(){
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["companies"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["companiesList"]
                    //let history = resultArray["values"]
                    let selec = "Selecciona..."
                    self.typesCompanies.append(selec)
                    for i in resultArray.arrayValue {
                        let createAt = i["name"].stringValue
                        self.typesCompanies.append(createAt)
                        
                    }
                    
                    self.companyTextField.text = self.typesCompanies.first
                    self.selectedTypeCompanies = self.typesCompanies.first!
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
}


@available(iOS 13.4, *)
extension FilterDataViewController {
    
    
    func showDatePicker(){
        //Formate Date
        inicialDatePicker.datePickerMode = .date
        finalDatePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(donedatePickerInicial));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerInicial));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateInicialTextField.inputAccessoryView = toolbar
        dateInicialTextField.inputView = inicialDatePicker
        inicialDatePicker.preferredDatePickerStyle = .wheels
        
        let toolbarFinal = UIToolbar();
        toolbarFinal.sizeToFit()
        let doneButtonFinal = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(donedatePickerFinal));
        let spaceButtonFinal = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonFinal = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerFinal));
        toolbarFinal.setItems([doneButtonFinal,spaceButtonFinal,cancelButtonFinal], animated: false)
        
        dateFinalTextField.inputAccessoryView = toolbarFinal
        dateFinalTextField.inputView = finalDatePicker
        finalDatePicker.preferredDatePickerStyle = .wheels
        
    }
    
    
    func iconsTextField(){
        statusFacturaTextField.rightViewMode = UITextField.ViewMode.always
        statusFacturaTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        profileTextField.rightViewMode = UITextField.ViewMode.always
        profileTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        companyTextField.rightViewMode = UITextField.ViewMode.always
        companyTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
    }
    
    func startDateOfMonth(){
        let dateFormatter = DateFormatter()
        let date = Date()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
        let startOfMonth = Calendar.current.date(from: comp)!
        print(dateFormatter.string(from: startOfMonth))
        dateInicialTextField.text! = dateFormatter.string(from: startOfMonth)
    }
    
    func currentDate(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let result = formatter.string(from: date)
        dateFinalTextField.text = result
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    func dismissPickerView() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        statusFacturaTextField.inputAccessoryView = toolBar
        
        let toolBarProfile = UIToolbar()
        toolBarProfile.sizeToFit()
        let buttonMethod = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarProfile.setItems([buttonMethod], animated: true)
        toolBarProfile.isUserInteractionEnabled = true
        profileTextField.inputAccessoryView = toolBarProfile
        
        let toolBarCompany = UIToolbar()
        toolBarCompany.sizeToFit()
        let buttonCFDI = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBarCompany.setItems([buttonCFDI], animated: true)
        toolBarCompany.isUserInteractionEnabled = true
        companyTextField.inputAccessoryView = toolBarCompany
        
    }
    
    @objc func donedatePickerInicial(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        dateInicialTextField.text = formatter.string(from: inicialDatePicker.date)
        print("fecha inicial seleccionada: \(dateInicialTextField.text!)")
        self.view.endEditing(true)
        
    }
    
    @objc func donedatePickerFinal(){
        let formatterFinal = DateFormatter()
        formatterFinal.dateFormat = "dd-MM-yyyy"
        dateFinalTextField.text = formatterFinal.string(from: finalDatePicker.date)
        print("fecha final seleccionada: \(dateFinalTextField.text!)")
        self.view.endEditing(true)
        
        
    }
    
    @objc func cancelDatePickerInicial(){
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePickerFinal(){
        self.view.endEditing(true)
    }
    
    func createPickerView() {
        
        statusPickerView.delegate = self
        statusFacturaTextField.inputView = statusPickerView
        
        profilePickerView.delegate = self
        profileTextField.inputView = profilePickerView
        
        CompanyPickerView.delegate = self
        companyTextField.inputView = CompanyPickerView
        
    }
    
}
