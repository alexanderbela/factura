//
//  MenuViewController.swift
//  facturaeasy
//
//  Created by Baxten on 19/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Firebase
import MaterialComponents.MaterialActivityIndicator
import NVActivityIndicatorView
import GoogleMobileAds

class MenuViewController: UIViewController, GADBannerViewDelegate, GADInterstitialDelegate{
    
    var db: Firestore!
    var keyRetrieve = [String]()
    var valueRetrieve = [String]()
    var group_array = [String:Any]()
    var profileFilter = ""
    var ListFactureDataModelArray =  [ListFactureDataModel]()
    //var TypesDataModelArray = [TypesDataModel]()
    
    @IBOutlet weak var facturasDataTableView: UITableView!
    @IBOutlet weak var viewWithDataMenu: UIView!
    @IBOutlet weak var viewEmptyMenu: UIView!
    @IBOutlet weak var filterImage: UIImageView!
    @IBOutlet weak var buttonFilter: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var isValid = false
    var menuRequester = MenuRequest()
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    private var shouldDisplayAd = true
    private var isAdReady:Bool = false {
        didSet {
            if isAdReady && shouldDisplayAd {
                displayAd()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        
        
    }
    
    func setUp(){
        db = Firestore.firestore()
        //getData()
        getDataProfilesWithFacturas()
        //view.sendSubviewToBack(viewEmptyMenu)
        facturasDataTableView.delegate = self
        facturasDataTableView.dataSource = self
        self.facturasDataTableView.separatorStyle = .none
        addObserversMenu()
        addObserversMenuNewFactura()
        addObserversMenuNewFacturaRefresh()
        addObserversValidatePayment()
        print("entra a viewDidLoad MenuViewController");
        filterImage.image = filterImage.image?.withRenderingMode(.alwaysTemplate)
        filterImage.tintColor = UIColor.white
        buttonFilter.showsTouchWhenHighlighted = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("entra a viewWillAppear MenuViewController");
        //self.validPayment()
        
    }
    
    func loadLittleBanner(){
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-9763277647634316/9997824077"
        //bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //codigo de prueba
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9763277647634316/6832604681")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        print(#function, "ad ready", interstitial.isReady)
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print(#function, "ad ready", interstitial.isReady)
        isAdReady = true
    }
    
    private func displayAd() {
        print(#function, "ad ready", interstitial.isReady)
        if (interstitial.isReady) {
            shouldDisplayAd = false
            interstitial.present(fromRootViewController: self)
        }
    }
    
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        // Gets the domain from which the error came.
        let errorDomain = error.domain
        // Gets the error code. See
        // https://developers.google.com/admob/ios/api/reference/Enums/GADErrorCode
        // for a list of possible codes.
        let errorCode = error.code
        // Gets an error message.
        // For example "Account not approved yet". See
        // https://support.google.com/admob/answer/9905175 for explanations of
        // common errors.
        let errorMessage = error.localizedDescription
        // Gets additional response information about the request. See
        // https://developers.google.com/admob/ios/response-info for more information.
        let responseInfo = error.userInfo[GADErrorUserInfoKeyResponseInfo] as? GADResponseInfo
        // Gets the underlyingError, if available.
        let underlyingError = error.userInfo[NSUnderlyingErrorKey] as? Error
        if let responseInfo = responseInfo {
            print("Received error with domain: \(errorDomain), code: \(errorCode),"
                    + "message: \(errorMessage), responseInfo: \(responseInfo),"
                    + "underLyingError: \(underlyingError?.localizedDescription ?? "nil")")
        }
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        print("entra a viewWillDisappear MenuViewController");
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @available(iOS 13.4, *)
    @IBAction func filterButton(_ sender: UIButton) {
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterDataViewController") as! FilterDataViewController
        //yourVc.modalPresentationStyle = .overFullScreen
        self.present(yourVc, animated: true, completion: nil)
        
    }
    
    func getDataProfilesWithFacturas(){
        
        self.loadingActivityIndicator.startAnimating()
        menuRequester.getDataFirebase(){ result in
            self.loadingActivityIndicator.stopAnimating()
            switch result {
            case .success(let arrayProfile):
                
                self.ListFactureDataModelArray = arrayProfile
                self.updateViews(arrayProfile: self.ListFactureDataModelArray)
                print("Cantidad de facturas: \(self.ListFactureDataModelArray.count)")
                let cantidad = self.ListFactureDataModelArray.count
                let item = AmountFacturasDataModel(cantidad: cantidad)
                Singleton.shared.setCantidadFacturas(name: item)
                self.facturasDataTableView.reloadData()
                self.validPayment()
                break;
            case .failure(_):
                let alert = UIAlertController(title: "", message: "¡Verifique su conexión a internet!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                self.present(alert, animated: true)
                break;
            }
        }
        
    }
    
    func updateViews(arrayProfile: [ListFactureDataModel]){
        if(arrayProfile.isEmpty == true){
            self.view.sendSubviewToBack(self.loadingView)
            self.view.sendSubviewToBack(self.viewWithDataMenu)
            self.view.bringSubviewToFront(self.viewEmptyMenu)
        }
        else{
            self.view.sendSubviewToBack(self.loadingView)
            self.view.sendSubviewToBack(self.viewEmptyMenu)
            self.view.bringSubviewToFront(self.viewWithDataMenu)
        }
        
    }
    
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.ListFactureDataModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FacturasDataTableViewCell", for: indexPath) as! FacturasDataTableViewCell
        
        let elements = self.ListFactureDataModelArray[indexPath.row]
        cell.companyFacturaLabel.text = elements.company
        cell.aliasFacturaLabel.text = elements.profile["alias"] as? String
        //cell.amountFacturaLabel.text = amountData(amount: self.amount[indexPath.row])
        cell.amountFacturaLabel.text = (elements.amount == "") ? elements.amount : "$" + elements.amount
        cell.dateFacturaLabel.text = elements.serverTimestamp
        cell.imageStatusFactura.image = imagePosition(status: elements.status)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FacturaDetailsViewController") as! FacturaDetailsViewController
        let elements = self.ListFactureDataModelArray[indexPath.row]
        yourVc.getData = elements
        yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
    }
    
    func imagePosition(status: String) -> UIImage{
        
        var statusReturn = UIImage(named: "ic_invoice_failed")!
        if(status == "COMPLETED" || status == "COMPLETE"){
            statusReturn = UIImage(named: "ic_invoice_success")!
        }
        
        if(status == "INCOMPLETE"){
            statusReturn = UIImage(named: "ic_invoice_incomplete")!
        }
        
        if(status == "PENDING"){
            statusReturn = UIImage(named: "ic_invoice_pending")!
        }
        
        if(status == "FAILED"){
            statusReturn = UIImage(named: "ic_invoice_failed")!
        }
        return statusReturn;
        
    }
    
}

extension MenuViewController {
    
    func addObserversMenu() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .addFilterFactura, object: nil)
    }
    
    @objc func notificationReceived(_ notification: Notification) {
        
        self.ListFactureDataModelArray.removeAll()
        guard let dateInicial = notification.userInfo?["dateInicial"] as? String else { return }
        guard let dateFinal = notification.userInfo?["dateFinal"] as? String else { return }
        guard let statusFactura = notification.userInfo?["statusFactura"] as? String else { return }
        guard let profile = notification.userInfo?["profile"] as? String else { return }
        guard let companies = notification.userInfo?["companies"] as? String else { return }
        print("-------------------------------------------------------")
        print ("dateInicial menuViewController: \(dateInicial)")
        print ("dateFinal menuViewController: \(dateFinal)")
        print ("statusFactura menuViewController: \(statusFactura)")
        print ("profile menuViewController: \(profile)")
        print ("companies menuViewController: \(companies)")
        getDataProfilesWithFacturasWithFilters(status: statusFactura, profileName: profile, companyName: companies, dateStart: dateInicial, dateEnd: dateFinal)
        
    }
    
    func getDataProfilesWithFacturasWithFilters(status: String, profileName: String, companyName: String, dateStart: String, dateEnd: String){
        
        let dateFormatter = DateFormatter()
        let date = Date()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
        let startOfMonth = Calendar.current.date(from: comp)!//este es el bueno Date
        
        let userID = Auth.auth().currentUser!.uid
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let fechaInicial = dateFormatter.date(from: dateStart)
        let fechaFinal = dateFormatter.date(from: dateEnd)
        
        self.db.collection("users").document(userID).collection("invoices").order(by: "serverTimestamp", descending: true).getDocuments { (snapshot, err) in
            
            if let err = err {
                //self.removeSpinner()
                print("Error getting documents: \(err)")
            } else {
                //self.removeSpinner()
                
                for document in snapshot!.documents {
                    
                    //print("fecha date inicial: \(fechaInicial!)")
                    let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                    let dateServer: Date = serverTimestamp.dateValue()
                    //print("fecha server dateServer: \(dateServer)")
                    //let fechaStartMonth = dateFormatter.string(from: dateServer)
                    //print("fecha server: \(fechaStartMonth)")
                    let company = document.get("company") as! String
                    let statusServer = document.get("status") as! String
                    
                    let profiles = document.get("profile") as! [String:Any]
                    
                    //self.group_array = (document["profile"] as? [String:Any])!
                    do {
                        
                        //print("group_array: \(self.group_array)")
                        let jsonData = try JSONSerialization.data(withJSONObject: profiles, options: [])
                        let myresult = try? JSON(data: jsonData)
                        //print("myresult: \(myresult)")
                        self.profileFilter = myresult!["alias"].description
                        //self.alias.append(resultNameAliasServer)
                        
                    } catch
                    {
                        print(error.localizedDescription)
                        
                    }
                    
                    //empieza primer if
                    if( fechaInicial! <= dateServer && fechaFinal! >= dateServer && company == companyName && status == statusServer && self.profileFilter == profileName){
                        print("entra primer if")
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        self.ListFactureDataModelArray.append(item)
                        
                    }//termina primer if
                    
                    //empieza segundo if
                    
                    if( fechaInicial! <= dateServer && fechaFinal! >= dateServer && company == companyName && status == statusServer && profileName == "Selecciona..."){
                        print("entra segundo if")
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        self.ListFactureDataModelArray.append(item)
                    }//termina segundo if
                    
                    //empieza tercer if
                    if( fechaInicial! <= dateServer && fechaFinal! >= dateServer && company == companyName && status == "Selecciona..." && profileName == "Selecciona..."){
                        print("entra tercer if")
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        self.ListFactureDataModelArray.append(item)
                    }//termina tercer if
                    
                    
                    //empieza cuarto if
                    if( fechaInicial! <= dateServer && fechaFinal! >= dateServer && companyName == "Selecciona..." && status == "Selecciona..." && profileName == "Selecciona..."){
                        print("entra tercer if")
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        self.ListFactureDataModelArray.append(item)
                    }//termina cuarto if
                    
                    //empieza quinto if
                    if( fechaInicial! <= dateServer && fechaFinal! >= dateServer && companyName == "Selecciona..." && status == statusServer && profileName == "Selecciona..."){
                        print("entra tercer if")
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        self.ListFactureDataModelArray.append(item)
                    }//termina quinto if
                    
                    
                    //empieza sexto if
                    if( fechaInicial! <= dateServer && fechaFinal! >= dateServer && companyName == "Selecciona..." && status == "Selecciona..." && profileName == self.profileFilter){
                        print("entra tercer if")
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        self.ListFactureDataModelArray.append(item)
                    }//termina sexto if
                    
                }
                
                
                if(self.ListFactureDataModelArray.isEmpty == true){
                    self.view.sendSubviewToBack(self.viewWithDataMenu)
                    self.view.bringSubviewToFront(self.viewEmptyMenu)
                }
                else{
                    self.view.sendSubviewToBack(self.viewEmptyMenu)
                    self.view.bringSubviewToFront(self.viewWithDataMenu)
                }
                
                self.facturasDataTableView.reloadData()
                
                
            }
        }
    }
    
    /*
    func validPaymentSingleton(){
        
        let isValid = Singleton.shared.getIsValidPayment()?.isValid
        print("isValid account: \(isValid!)")
        let cantidadFacturas = Singleton.shared.getCantidadFacturas()?.cantidad
        //amountFacturas.text = String(10 - cantidadFacturas!)
        let saldo = 10 - cantidadFacturas!
        if saldo != 0 {
            interstitial = createAndLoadInterstitial()
            self.displayAd()
            self.loadLittleBanner()
            self.addBannerViewToView(self.bannerView)
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
        }else{
            if isValid == false {
                self.interstitial = self.createAndLoadInterstitial()
                self.displayAd()
                self.loadLittleBanner()
                self.addBannerViewToView(self.bannerView)
            }
            
        }
    }
 */
    
    
    func validPayment(){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        let cantidadFacturas = Singleton.shared.getCantidadFacturas()?.cantidad
        //amountFacturas.text = String(10 - cantidadFacturas!)
        let saldo = 10 - cantidadFacturas!
        if saldo != 0 {
            
            interstitial = createAndLoadInterstitial()
            self.displayAd()
            self.loadLittleBanner()
            self.addBannerViewToView(self.bannerView)
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
        }else{
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
            let docRef = db.collection("validateMonthlyPaymentByUser").document(userID)
            
            docRef.getDocument { (document, error) in
                
                if let document = document, document.exists {
                    
                    let serverTimestamp = document.get("timeStampEnd") as! String
                    let date = NSDate(timeIntervalSince1970: Double(serverTimestamp)!)
                    let dateTod = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"
                    
                    let dateToday2 = formatter.string(from: dateTod)
                    let dateServer2 = formatter.string(from: date as Date)
                    
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                    dateFormatter2.dateFormat = "dd-MM-yyyy"
                    let dateToday = dateFormatter2.date(from:dateToday2)
                    let dateServer = dateFormatter2.date(from:dateServer2)
                    
                    print("dateToday: \(dateToday!)")
                    print("dateServer: \(dateServer!)")
                    
                    if(dateServer! <= dateToday!){
                        
                        let isValid = false
                        print("Tu suscripción ha vencido")
                        
                        let isVal = isValidPayment(isValid: isValid)
                        Singleton.shared.setIsValidPayment(name: isVal)
                        print("isValid: \(isValid)")
                        self.interstitial = self.createAndLoadInterstitial()
                        self.displayAd()
                        self.loadLittleBanner()
                        self.addBannerViewToView(self.bannerView)
                        
                    }else{
                        
                        let isValid = true
                        print("Tu suscripción sigue vigente")
                        let isVal = isValidPayment(isValid: isValid)
                        Singleton.shared.setIsValidPayment(name: isVal)
                        print("isValid: \(isValid)")
                        //self.validatePaymentObserver(isValidPayment: isValid)
                        
                    }
                    
                } else {
                    
                    //isValid = false
                    let isValid = false
                    print("Tu suscripción ha vencido")
                    let isVal = isValidPayment(isValid: isValid)
                    Singleton.shared.setIsValidPayment(name: isVal)
                    
                    print("Document does not exist")
                    self.interstitial = self.createAndLoadInterstitial()
                    self.displayAd()
                    self.loadLittleBanner()
                    self.addBannerViewToView(self.bannerView)
                }
            }
        }
        
    }
}

extension MenuViewController {
    
    func addObserversMenuNewFactura() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceivedNewFactura), name: .addNewFactura, object: nil)
    }
    
    @objc func notificationReceivedNewFactura(_ notification: Notification) {
        
        self.ListFactureDataModelArray.removeAll()
        self.getDataProfilesWithFacturas()
        
    }
    
}

extension MenuViewController {
    
    func addObserversMenuNewFacturaRefresh() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceivedNewFacturaRefresh), name: .addNewFacturaRefresh, object: nil)
    }
    
    @objc func notificationReceivedNewFacturaRefresh(_ notification: Notification) {
        
        self.ListFactureDataModelArray.removeAll()
        getDataProfilesWithFacturas()
        
        
    }
    
    func addObserversValidatePayment() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceivedValidationPayment), name: .validPayment, object: nil)
    }
    
    @objc func notificationReceivedValidationPayment(_ notification: Notification) {
        self.getDataProfilesWithFacturas()
    }
    
}


