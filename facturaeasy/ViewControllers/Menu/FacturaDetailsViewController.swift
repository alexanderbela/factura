//
//  FacturaDetailsViewController.swift
//  facturaeasy
//
//  Created by Baxten on 11/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON


class ValuesInformationDetail: Codable{
    var key: String = ""
    var type: String = ""
    var value: String = ""
    
    init(key: String, type: String ,value: String) {
        self.key = key
        self.type = type
        self.value = value
    }
}

class FacturaDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate {
    
    
    
    @IBOutlet weak var aliasDetailFacturaLabel: UILabel!
    @IBOutlet weak var rfcDetailFacturaLabel: UILabel!
    @IBOutlet weak var companyFacturaDetailLabel: UILabel!
    @IBOutlet weak var useCFDIDetailLabel: UILabel!
    @IBOutlet weak var paymentMethodDetailLabel: UILabel!
    
    //---------- components data completed -------------------
    @IBOutlet weak var aliasCompletedLabel: UILabel!
    @IBOutlet weak var rfcCompletedLabel: UILabel!
    @IBOutlet weak var companyCompletedLabel: UILabel!
    @IBOutlet weak var montoCompletedLabel: UILabel!
    @IBOutlet weak var fechaCompletedLabel: UILabel!
    @IBOutlet weak var usoCFDICompletedLabel: UILabel!
    @IBOutlet weak var paymentCompletedLabel: UILabel!
    //--------------------------------------------------------
    
    //---------- components data incomplete -------------------
    @IBOutlet weak var companyPendingPickerTextField: UITextField!
    @IBOutlet weak var ticketPendingImageView: UIImageView!
    @IBOutlet weak var useCFDIIncompletePickerTextField: UITextField!
    @IBOutlet weak var paymentIncompletePicketTextField: UITextField!
    @IBOutlet weak var fourDigitsCardTextField: UITextField!
    @IBOutlet weak var hideIncompleteLabel: UILabel!
    @IBOutlet weak var heightViewIncomplete: NSLayoutConstraint!
    @IBOutlet weak var aliasDataPickerTextField: UITextField!
    
    //--------------------------------------------------------
    
    @IBOutlet weak var heightViewPrincipal: NSLayoutConstraint!
    @IBOutlet weak var heightTicketView: NSLayoutConstraint!
    @IBOutlet weak var hideLabel: UILabel!
    @IBOutlet weak var ticketFacturaDetailImageView: UIImageView!
    @IBOutlet var principalDetailFacturaView: UIView!
    
    
    @IBOutlet weak var statusPendingView: UIView!
    @IBOutlet weak var statusCompletedView: UIView!
    @IBOutlet weak var statusIncompleteView: UIView!
    @IBOutlet weak var viewWithFourDigits: UIView!
    @IBOutlet weak var heightViewFourDigits: NSLayoutConstraint!
    @IBOutlet weak var heightWithPaymenteData: NSLayoutConstraint!
    
    
    @IBOutlet weak var heightCompletedview: NSLayoutConstraint!
    
    @IBOutlet weak var scrollViewDatas: UIScrollView!
    
    var db: Firestore!
    
    var iconClick = true
    var iconClickIncomplete = true
    
    var getData:ListFactureDataModel!
    
    var keyInformation = [String]()
    var valueInformation = [String]()
    
    var keyInformationIncomplete = [String]()
    var valueInformationIncomplete = [String]()
    var typeInformationIncomplete = [String]()
    
    var tamañoElementsInformation: Int = 0
    
    var validation = Validation()
    
    var valuesInformationArrayDetail = [ValuesInformationDetail]()
    
    
    @IBOutlet weak var tableInformation: UITableView!
    @IBOutlet weak var tableInformationCompleted: UITableView!
    @IBOutlet weak var tableInformationIncomplete: UITableView!
    
    var pickerViewCompaniesIncomplete = UIPickerView()
    var pickerViewPaymentIncomplete = UIPickerView()
    var pickerViewUseCFDIIncomplete = UIPickerView()
    var pickerViewAlias = UIPickerView()
    
    var typesCompaniesIncomplete = [String]()
    var typesPaymentIncomplete = [String]()
    var typesUseCFDIIncomplete = [String]()
    var typeAliasProfileIncomplete = [String]()
    var idProfileIncomplete = [String]()
    
    var selectedCompaniesRowIncomplete: Int = 0
    var selectedMethodRowIncomplete: Int = 0
    var selectedCFDIRowIncomplete: Int = 0
    var selectedAliasProfileRowIncomplete: Int = 0
    
    var rowSelectedIncomplete: String = ""
    
    var selectedCompaniesIncomplete: String?
    var selectedUseCFDIIncomplete: String?
    var selectedPaymentIncomplete: String?
    var selectedAliasProfileIncomplete: String?
    var selectedIdProfileIncomplete: String?
    
    var typesCountIncomplete: Int = 0;
    
    var valuesData = [JSON]()
    var displayNameIncomplete = [String]()
    //var valueInformationIncomplete = [String]()
    
    var tagData = ""
    var getDataProfile = [ProfileDataModel]()
    
    @IBOutlet weak var rfcIncompleteStatus: UILabel!
    
    var invoicesValues = ""
    var amount = ""
    
    override func viewDidLoad() {
        db = Firestore.firestore()
        print("status: " + getData.status)
        
        
        tableInformation.delegate = self
        tableInformation.dataSource = self
        tableInformationCompleted.delegate = self
        tableInformationCompleted.dataSource = self
        tableInformationIncomplete.delegate = self
        tableInformationIncomplete.dataSource = self
        
        self.tableInformation.separatorStyle = .none
        self.tableInformationCompleted.separatorStyle = .none
        self.tableInformationIncomplete.separatorStyle = .none
        
        createPickerView()
        dismissPickerView()
        
        getDataDetailFactura()
        
        getDataImageFactura(documentID: getData.id)
        
        //self.getDataMethods(position: paymentMethod)
        //self.getDataCFDI(position: usoCFDI)
        iconsTextField()
        self.dismissKey()
        print("Entra a viewdidload FacturaDetailsViewController:")
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return ticketPendingImageView
    }
    
    func iconsTextField(){
        
        paymentIncompletePicketTextField.rightViewMode = UITextField.ViewMode.always
        paymentIncompletePicketTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        useCFDIIncompletePickerTextField.rightViewMode = UITextField.ViewMode.always
        useCFDIIncompletePickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        companyPendingPickerTextField.rightViewMode = UITextField.ViewMode.always
        companyPendingPickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        aliasDataPickerTextField.rightViewMode = UITextField.ViewMode.always
        aliasDataPickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.pickerViewCompaniesIncomplete {
            typesCountIncomplete = typesCompaniesIncomplete.count
        }
        
        if pickerView == self.pickerViewUseCFDIIncomplete {
            typesCountIncomplete = typesUseCFDIIncomplete.count
        }
        
        if pickerView == self.pickerViewPaymentIncomplete {
            typesCountIncomplete = typesPaymentIncomplete.count
        }
        
        if pickerView == self.pickerViewAlias {
            typesCountIncomplete = typeAliasProfileIncomplete.count
        }
        
        return typesCountIncomplete
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.pickerViewCompaniesIncomplete {
            rowSelectedIncomplete = typesCompaniesIncomplete[row]
        }
        
        if pickerView == self.pickerViewUseCFDIIncomplete {
            rowSelectedIncomplete = typesUseCFDIIncomplete[row]
        }
        
        if pickerView == self.pickerViewPaymentIncomplete {
            rowSelectedIncomplete = typesPaymentIncomplete[row]
        }
        
        if pickerView == self.pickerViewAlias {
            rowSelectedIncomplete = typeAliasProfileIncomplete[row]
        }
        
        return rowSelectedIncomplete
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerViewCompaniesIncomplete {
            selectedCompaniesRowIncomplete = row
            selectedCompaniesIncomplete = typesCompaniesIncomplete[row] // selected item
            companyPendingPickerTextField.text = selectedCompaniesIncomplete
            print("selectedType: \(selectedCompaniesIncomplete!)")
            self.getInformationCompaniesIncomplete(name: selectedCompaniesIncomplete!)
            //getDataRetrieve(documentID: stringDocID)
        }
        
        if pickerView == pickerViewUseCFDIIncomplete {
            selectedCFDIRowIncomplete = row
            selectedUseCFDIIncomplete = typesUseCFDIIncomplete[row] // selected item
            useCFDIIncompletePickerTextField.text = selectedUseCFDIIncomplete
            print("selectedType: \(selectedUseCFDIIncomplete!)")
            
        }
        
        if pickerView == pickerViewPaymentIncomplete {
            selectedMethodRowIncomplete = row
            selectedPaymentIncomplete = typesPaymentIncomplete[row] // selected item
            paymentIncompletePicketTextField.text = selectedPaymentIncomplete
            print("selectedType: \(selectedPaymentIncomplete!)")
            if(selectedPaymentIncomplete == "Efectivo" || selectedPaymentIncomplete == "Tarjeta de servicios" || selectedPaymentIncomplete == "Cheque" || selectedPaymentIncomplete == "Transferencia electrónica de fondos"){
                viewWithFourDigits.isHidden = true
                heightViewFourDigits.constant = 0
                heightWithPaymenteData.constant = 200
                //heightViewIncomplete.constant = 550
                heightViewPrincipal.constant = 1310
            }
            else{
                viewWithFourDigits.isHidden = false
                heightViewFourDigits.constant = 108
                heightWithPaymenteData.constant = 300
                heightViewPrincipal.constant = 1410
            }
            
        }
        
        if pickerView == pickerViewAlias {
            selectedAliasProfileRowIncomplete = row
            selectedAliasProfileIncomplete = typeAliasProfileIncomplete[row] // selected item
            selectedIdProfileIncomplete = idProfileIncomplete[row]
            aliasDataPickerTextField.text = selectedAliasProfileIncomplete
            print("selectedType: \(selectedAliasProfileIncomplete!)")
            
            self.getRFC(alias: self.selectedAliasProfileIncomplete!)
            self.getDataProfileToInsideInvoices(profileID: selectedIdProfileIncomplete!)
            
        }
        
    }
    
    func createPickerView() {
        
        pickerViewCompaniesIncomplete.delegate = self
        companyPendingPickerTextField.inputView = pickerViewCompaniesIncomplete
        
        pickerViewUseCFDIIncomplete.delegate = self
        useCFDIIncompletePickerTextField.inputView = pickerViewUseCFDIIncomplete
        
        pickerViewPaymentIncomplete.delegate = self
        paymentIncompletePicketTextField.inputView = pickerViewPaymentIncomplete
        
        pickerViewAlias.delegate = self
        aliasDataPickerTextField.inputView = pickerViewAlias
        
    }
    
    func dismissPickerView() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        companyPendingPickerTextField.inputAccessoryView = toolBar
        
        let toolBar2 = UIToolbar()
        toolBar2.sizeToFit()
        let button2 = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar2.setItems([button2], animated: true)
        toolBar2.isUserInteractionEnabled = true
        useCFDIIncompletePickerTextField.inputAccessoryView = toolBar2
        
        let toolBar3 = UIToolbar()
        toolBar3.sizeToFit()
        let button3 = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar3.setItems([button3], animated: true)
        toolBar3.isUserInteractionEnabled = true
        paymentIncompletePicketTextField.inputAccessoryView = toolBar3
        
        let toolBar4 = UIToolbar()
        toolBar4.sizeToFit()
        let button4 = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar4.setItems([button4], animated: true)
        toolBar4.isUserInteractionEnabled = true
        aliasDataPickerTextField.inputAccessoryView = toolBar4
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var tamaño = 0;
        
        if tableView == tableInformation {
            tamaño = self.keyInformation.count
        }
        
        if tableView == tableInformationCompleted {
            tamaño = self.keyInformation.count
            
        }
        
        if tableView == tableInformationIncomplete {
            tamaño = self.keyInformationIncomplete.count
            //tamaño = self.valuesInformationArrayDetail.count
        }
        
        return tamaño
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print("cellForRowAt: \(indexPath)")
        var cellToReturn = UITableViewCell()
        
        if tableView == tableInformation {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InformationRequiredTableViewCell", for: indexPath) as! InformationRequiredTableViewCell
            //cell.companyFacturaLabel.text = self.company[indexPath.row]
            cell.nameKeyInformation.text = self.keyInformation[indexPath.row]
            cell.valueInformationLabel.text = self.valueInformation[indexPath.row]
            cellToReturn = cell
            
        }
        
        if tableView == tableInformationCompleted {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InformationCompletedTableViewCell", for: indexPath) as! InformationCompletedTableViewCell
            //cell.companyFacturaLabel.text = self.company[indexPath.row]
            cell.keyCompletedLabel.text = self.keyInformation[indexPath.row]
            cell.valueCompletedLabel.text = self.valueInformation[indexPath.row]
            cellToReturn = cell
        }
        
        
        if tableView == tableInformationIncomplete {
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
            //let item = valuesInformationArrayDetail[indexPath.row]
            
            
            print("key: \(self.keyInformationIncomplete[indexPath.row])")
            print("value: \(self.valueInformationIncomplete[indexPath.row])")
            cell.nameKeyIncompleteInformationLabel.text = self.keyInformationIncomplete[indexPath.row]
            cell.valueIncompleteInformationTextField.text = self.valueInformationIncomplete[indexPath.row]
            switch(indexPath.row){
            
            //cell.companyFacturaLabel.text = self.company[indexPath.row]
            
            case 0 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                
                //cell.nameKeyIncompleteInformationLabel.text = item.key
                //cell.valueIncompleteInformationTextField.tag = 0
                self.tagData = cell.valueIncompleteInformationTextField.text!
                break;
                
            case 1 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                cell.valueIncompleteInformationTextField.tag = 1
                break;
                
            case 2 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                cell.valueIncompleteInformationTextField.tag = 2
                break;
                
            case 3 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                cell.valueIncompleteInformationTextField.tag = 3
                break;
            case 4 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                cell.valueIncompleteInformationTextField.tag = 4
                break;
            case 5 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                cell.valueIncompleteInformationTextField.tag = 5
                break;
            case 6 :
                cell = tableView.dequeueReusableCell(withIdentifier: "InformationIncompleteTableViewCell", for: indexPath) as! InformationIncompleteTableViewCell
                cell.configure(lblValue: self.keyInformationIncomplete[indexPath.row], txtValue: self.valueInformationIncomplete[indexPath.row])
                cell.valueIncompleteInformationTextField.tag = 6
                break;
                
            default :
                break;
            }
            
            cellToReturn = cell
            editChangeDetail(cell.valueIncompleteInformationTextField)
            
        }
        
        return cellToReturn
    }
    
    
    @IBAction func editChangeDetail(_ sender: UITextField) {
        
        if(sender.text == ""){
            return
        }
        let pos = sender.tag
        
        let item = self.valuesInformationArrayDetail[pos]
        item.value = sender.text!
        
        if(item.key == "Monto" || item.key == "Total"){
            self.amount = item.value
            print("amount: \(self.amount)")
            //self.amount =
        }
        
        
        //print("self.myresult2: \(self.myresult2!)")
        print("pos: \(item)")
        print("item.value: \(item.value)")
        
        
    }
    
    
    
    func getDataRetrieve(documentID: String){
        
        print("entra a getDataRetrieve")
        print("documentID: \(documentID)")
        let userID = Auth.auth().currentUser!.uid
        
        self.db.collection("users").document(userID).collection("invoices").whereField("id",isEqualTo: documentID).getDocuments { (snapshot, err) in
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                for document in snapshot!.documents {
                    
                    let invoiceValues = document.get("invoiceValues") as! String
                    print("invoiceValues: \(invoiceValues)")
                    // Convert your response string to data or if you've data then pass it directly
                    let jsonData = invoiceValues.data(using: .utf8)
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: jsonData!, options: [])
                        if let array = json as? [[String : AnyObject]] {
                            let tamaño = array.count
                            print("tamaño arreglo: \(tamaño)")
                            self.tamañoElementsInformation = tamaño
                            for obj in array {
                                let distName = obj["key"]
                                print("key: \(distName!)")
                                let type = obj["type"]
                                print("key: \(type!)")
                                self.keyInformation.append(distName! as! String)
                                let value = obj["value"]
                                print("value: \(value!)")
                                self.valueInformation.append(value! as! String)
                                
                                if(distName! as! String  == "Total" || distName! as! String  == "Monto"){
                                    let va = value as! String
                                    //self.montoCompletedLabel.text = va
                                }
                                
                                if(distName! as! String  == "Fecha"){
                                    let va = value as! String
                                    //self.fechaCompletedLabel.text = va
                                }
                                
                            }
                        }
                        
                        
                    }
                    catch {
                        print("Couldn't parse json \(error)")
                    }
                    
                    //self.displayNameIncomplete = self.keyInformation
                    //self.valueInformationIncomplete = self.valueInformation
                    
                    self.tableInformation.reloadData()
                    self.tableInformationCompleted.reloadData()
                    
                    print("self.keyInformation: \(self.keyInformation)")
                    print("self.valueInformation: \(self.valueInformation)")
                }
            }
        }
    }
    
    func getDataRetrieveIncomplete(documentID: String){
        
        print("entra a getDataRetrieve")
        print("documentID: \(documentID)")
        let userID = Auth.auth().currentUser!.uid
        
        self.db.collection("users").document(userID).collection("invoices").whereField("id",isEqualTo: documentID).getDocuments { (snapshot, err) in
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                for document in snapshot!.documents {
                    
                    let invoiceValues = document.get("invoiceValues") as! String
                    print("invoiceValues: \(invoiceValues)")
                    // Convert your response string to data or if you've data then pass it directly
                    let jsonData = invoiceValues.data(using: .utf8)
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: jsonData!, options: [])
                        if let array = json as? [[String : AnyObject]] {
                            let tamaño = array.count
                            print("tamaño arreglo: \(tamaño)")
                            //self.tamañoElementsInformation = tamaño
                            for obj in array {
                                let displayName = obj["key"] as! String
                                print("key: \(displayName)")
                                self.keyInformationIncomplete.append(displayName)
                                let type = obj["type"] as! String
                                print("type: \(type)")
                                self.typeInformationIncomplete.append(type)
                                
                                let value = obj["value"]  as! String
                                print("value: \(value)")
                                self.valueInformationIncomplete.append(value)
                                
                                if(displayName as! String  == "Total" || displayName as! String  == "Monto"){
                                    let va = value as! String
                                    //self.montoCompletedLabel.text = va
                                }
                                
                                if(displayName as! String  == "Fecha"){
                                    let va = value as! String
                                    //self.fechaCompletedLabel.text = va
                                }
                                
                                let item = ValuesInformationDetail(key: displayName, type: type, value: value)
                                self.valuesInformationArrayDetail.append(item)
                                
                            }
                        }
                        
                    }
                    catch {
                        print("Couldn't parse json \(error)")
                    }
                    
                    //self.displayNameIncomplete = self.keyInformation
                    //self.valueInformationIncomplete = self.valueInformation
                    
                    
                    self.tableInformationIncomplete.reloadData()
                    //self.tableInformationIncomplete.reloadData()
                    print("self.keyInformation: \(self.keyInformation)")
                    print("self.valueInformation: \(self.valueInformation)")
                }
            }
        }
    }
    
    
    func getDataImageFactura(documentID: String){
        
        let userID = Auth.auth().currentUser!.uid
        //print("documentID: \(documentID)")
        self.showSpinner(onView: view)
        //self.db.collection("users").document(userID).collection("invoices").whereField("created", isGreaterThanOrEqualTo:
        self.db.collection("users").document(userID).collection("invoice_images").whereField("invoiceId",isEqualTo: documentID).getDocuments { (snapshot, err) in
            
            if let err = err {
                self.removeSpinner()
                print("Error getting documents: \(err)")
            } else {
                
                self.removeSpinner()
                for document in snapshot!.documents {
                    
                    //let image = document.get("image") as! String
                    //print("image: \(image)")
                    let image = document.get("image") as! String
                    //print("image: \(image)")
                    
                    let imagen = image.toImage() // it will convert String  to UIImage
                    
                    print("image ticket: \(imagen!)")
                    //let imagen = image.jpegData(compressionQuality: 1)
                    self.ticketFacturaDetailImageView.image = imagen
                    self.ticketPendingImageView.image = imagen
                    
                }
                
            }
        }
    }
    
    func getDataDetailFactura(){
        
        if(getData.status == "PENDING"){
            print("entra if pending")
            getDataRetrieve(documentID: getData.id)
            aliasDetailFacturaLabel.text = (getData.profile["alias"] as! String)
            rfcDetailFacturaLabel.text = (getData.profile["rfc"] as! String)
            companyFacturaDetailLabel.text = getData.company
            useCFDIDetailLabel.text = getData.cdfi
            paymentMethodDetailLabel.text = getData.paymentMethod
            
            self.scrollViewDatas.sendSubviewToBack(self.statusIncompleteView)
            self.scrollViewDatas.sendSubviewToBack(self.statusCompletedView)
            self.scrollViewDatas.bringSubviewToFront(self.statusPendingView)
            
            heightViewPrincipal.constant = 1273
            statusCompletedView.isHidden = true
            statusPendingView.isHidden = false
            statusIncompleteView.isHidden = true
            
            self.backgroundImageView(data: self.getData.profile["selectedImagePosition"] as! Int)
            
        }
        
        if(getData.status == "COMPLETED" || getData.status == "COMPLETE"){
            print("entra if completed")
            getDataRetrieve(documentID: getData.id)
            aliasCompletedLabel.text = (getData.profile["alias"] as! String)
            rfcCompletedLabel.text = (getData.profile["rfc"] as! String)
            companyCompletedLabel.text = getData.company
            usoCFDICompletedLabel.text = getData.cdfi
            paymentCompletedLabel.text = getData.paymentMethod
            //fechaCompletedLabel.text = fechaTicket
            
            self.scrollViewDatas.sendSubviewToBack(self.statusPendingView)
            self.scrollViewDatas.sendSubviewToBack(self.statusIncompleteView)
            self.scrollViewDatas.bringSubviewToFront(self.statusCompletedView)
            heightCompletedview.constant = 820
            heightViewPrincipal.constant = 820
            
            statusCompletedView.isHidden = false
            statusPendingView.isHidden = true
            statusIncompleteView.isHidden = true
            self.backgroundImageView(data: Int(getData.selectedImagePosition)!)
            
        }
        
        if(getData.status == "INCOMPLETE"){
            
            getDataRetrieveIncomplete(documentID: getData.id)
            getDataCompaniesNameIncomplete()
            getDataMethods()
            getDataCFDI()
            getDataProfilesListIncomplete()
            print("entra if incomplete")
            aliasDataPickerTextField.text = (getData.profile["alias"] as! String)
            rfcCompletedLabel.text = (getData.profile["rfc"] as! String)
            //fechaCompletedLabel.text = fechaTicket
            fourDigitsCardTextField.text = getData.cardNumber
            
            self.scrollViewDatas.sendSubviewToBack(self.statusPendingView)
            self.scrollViewDatas.sendSubviewToBack(self.statusCompletedView)
            self.scrollViewDatas.bringSubviewToFront(self.statusIncompleteView)
            heightCompletedview.constant = 1440
            heightViewPrincipal.constant = 1440
            
            statusCompletedView.isHidden = true
            statusPendingView.isHidden = true
            statusIncompleteView.isHidden = false
            //self.backgroundImageView(data: Int(imagePosition)!)
        }
    }
    
    func backgroundImageView(data: Int){
        //let data = indexPath.item
        print("data position image: \(data)")
        switch data {
        case 0:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
        case 1:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_city_background")!)
        case 2:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_mountain_background")!)
        case 3:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_pyramid_background")!)
        case 4:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_snow_background")!)
        case 5:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_space_background")!)
            
        default:
            self.principalDetailFacturaView.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
        }
    }
    
    @IBAction func showTicketButton(_ sender: UIButton) {
        
        let ver = "Ver"
        let ocultar = "Ocultar"
        
        if(iconClick == true) {
            heightTicketView.constant = 35
            heightViewPrincipal.constant = 750
            ticketFacturaDetailImageView.isHidden = true
            hideLabel.text = ver;
            
        } else {
            heightTicketView.constant = 550
            heightViewPrincipal.constant = 1273
            ticketFacturaDetailImageView.isHidden = false
            hideLabel.text = ocultar;
        }
        iconClick = !iconClick
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func showTicketIncomplete(_ sender: UIButton) {
        
        let ver = "Ver"
        let ocultar = "Ocultar"
        
        if(iconClickIncomplete == true) {
            heightViewIncomplete.constant = 35
            heightViewPrincipal.constant = 900
            ticketPendingImageView.isHidden = true
            hideLabel.text = ver;
            
        } else {
            heightViewIncomplete.constant = 550
            heightViewPrincipal.constant = 1410
            ticketPendingImageView.isHidden = false
            hideLabel.text = ocultar;
        }
        iconClickIncomplete = !iconClickIncomplete
        
    }
    
    
    @IBAction func facturarButton(_ sender: UIButton) {
        
        let company = self.selectedCompaniesIncomplete
        print("company: \(company!)")
        let fourDigits = fourDigitsCardTextField.text
        print("fourDigits: \(fourDigits!)")
        let cfdi = self.selectedUseCFDIIncomplete
        print("cfdi: \(cfdi!)")
        let payment = self.selectedPaymentIncomplete
        print("payment: \(payment!)")
        
        let userID = Auth.auth().currentUser!.uid
        let ref = db.collection("users")
        let docId = ref.document().documentID
        print("docID: \(docId)")
        
        var status = ""
        
        if(payment == "Efectivo" || payment == "Tarjeta de servicios" || payment == "Cheque" || payment == "Transferencia electrónica de fondos"){
            status = "PENDING"
        }
        else{
            if (fourDigits == ""){
                status = "INCOMPLETE"
            }else{
                let isValidFour = self.validation.validateFourDigitsCard(four: fourDigits!)
                if (isValidFour == false) {
                    print("Por favor ingresa los 4 últimos dígitos de tu tarjeta correctamente")
                    showToast(message: "Por favor ingresa los 4 últimos dígitos de tu tarjeta")
                    return
                }
                status = "PENDING"
            }
        }
        
        for i in 0...self.valuesInformationArrayDetail.count - 1{
            
            let j = self.valuesInformationArrayDetail[i]
            print("value information validation: \(j.value)")
            if (j.value == ""){
                showToast(message: "Por favor ingresa la información requerida por la empresa")
                return
            }
            else{
                continue
            }
            
        }
        
        
        do {
            
            let encodedDictionary = try JSONEncoder().encode(self.valuesInformationArrayDetail)
            print("encodedDictionary: \(encodedDictionary)")
            let result = try? JSON(data: encodedDictionary)
            print("myresult2: \(result!)")
            
            let str = String(decoding: encodedDictionary, as: UTF8.self)
            print("str: \(str)")
            
            self.invoicesValues = str
            
            
        } catch {
            print("Error: ", error)
        }
        
        let docData: [String: Any] = [
            
            "amount": self.amount,
            "cardNumber": fourDigits!,
            "cdfi": cfdi!,
            "company": company!,
            "date": "",
            "id": self.getData.id,
            "invoiceValues": self.invoicesValues,
            "paymentMethod": payment!,
            "retrievedText": getData.retrievedText,
            "serverTimestamp": Timestamp(date: Date()),
            "status": status ,
            "userId": userID,
            "profile": [
                "address": self.getDataProfile[0].address ,
                "alias": self.getDataProfile[0].alias,
                "defaultCdfi": self.getDataProfile[0].defaultCdfi,
                "defaultCompanyId": self.getDataProfile[0].defaultCompanyId,
                "defaultPaymentMethod": self.getDataProfile[0].defaultPaymentMethod,
                "email": self.getDataProfile[0].email,
                "enabled": true,
                "id": self.getDataProfile[0].idProfile,
                "lastNameFather": self.getDataProfile[0].lastNameFather,
                "lastNameMother": self.getDataProfile[0].lastNameMother,
                "municipality": self.getDataProfile[0].municipality,
                "name": self.getDataProfile[0].name,
                "phoneNumber": self.getDataProfile[0].phoneNumber,
                "postalCode": self.getDataProfile[0].postalCode,
                "rfc": self.getDataProfile[0].rfc,
                "selectedImagePosition": self.getDataProfile[0].imagePosition,
                "state": self.getDataProfile[0].state ,
                "type": self.getDataProfile[0].type
            ]
        ]
        
        
        db.collection("users").document(userID).collection("invoices").document(self.getData.id).setData(docData) { err in
            if let err = err {
                print("Error writing document: \(err)")
                //print("docData: \(docData)")
            } else {
                
                self.sendAddedNewPFacturaObserver()
                print("Document successfully written!")
                print("docData: \(docData)")
                self.dismiss(animated: true)
                
            }
        }
        
        
    }
    
    private func sendAddedNewPFacturaObserver(){
        NotificationCenter.default.post(name: .addNewFactura, object: nil)
        
    }
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString,
                             options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
}

extension String {
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters){
            return UIImage(data: data)
        }
        return nil
    }
}

extension FacturaDetailsViewController {
    
    func getDataCompaniesNameIncomplete(){
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["companies"] as? [String:Any]
                //print(group_array)
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["companiesList"]
                    
                    for i in resultArray.arrayValue {
                        
                        let createAt = i["name"].stringValue
                        self.typesCompaniesIncomplete.append(createAt)
                        
                    }
                    
                    print("company: \(self.getData.company)")
                    let indexOfA = self.typesCompaniesIncomplete.firstIndex(of: self.getData.company) // 0
                    if(indexOfA != nil){
                        print("indexOfA: \(indexOfA!)")
                        self.selectedCompaniesIncomplete = self.typesCompaniesIncomplete[indexOfA!]
                        self.companyPendingPickerTextField.text = self.typesCompaniesIncomplete[indexOfA!]
                        self.pickerViewCompaniesIncomplete.selectRow(indexOfA!, inComponent: 0, animated: true)
                        //self.getInformationCompaniesIncomplete(name: self.selectedCompaniesIncomplete!)
                        print("self.selectedCompaniesIncomplete! if: \(self.selectedCompaniesIncomplete!)")
                    }else{
                        self.selectedCompaniesIncomplete = self.typesCompaniesIncomplete.first
                        self.companyPendingPickerTextField.text = self.typesCompaniesIncomplete.first
                        //self.getInformationCompaniesIncomplete(name: self.selectedCompaniesIncomplete!)
                        print("self.selectedCompaniesIncomplete! else: \(self.selectedCompaniesIncomplete!)")
                    }
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    
    func getInformationCompaniesIncomplete(name: String){
        print("name: \(name)")
        self.valuesData.removeAll()
        self.keyInformationIncomplete.removeAll()
        self.valueInformationIncomplete.removeAll()
        self.valuesInformationArrayDetail.removeAll()
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["companies"] as? [String:Any]
                //print(group_array)
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["companiesList"]
                    //let history = resultArray["values"]
                    
                    for i in resultArray.arrayValue {
                        
                        let nameService = i["name"].stringValue
                        if(nameService == name){
                            print("name: \(name)")
                            print("entra if nameService == name")
                            let valores = i["values"].arrayValue
                            //print("valores: \(valores)")
                            self.valuesData.append(contentsOf: valores)
                            for i in self.valuesData {
                                //print(i)
                                
                                let displayName = i["displayName"].stringValue
                                self.keyInformationIncomplete.append(displayName)
                                let type = i["type"].stringValue
                                self.typeInformationIncomplete.append(type)
                                let value = i["value"].stringValue
                                self.valueInformationIncomplete.append(value)
                                
                                
                                let item = ValuesInformationDetail(key: displayName, type: type, value: value)
                                self.valuesInformationArrayDetail.append(item)
                                //print("item: \(item)")
                                
                            }
                            
                        }
                        
                    }
                    
                    print("self.valuesInformationArrayDetail: \(self.valuesInformationArrayDetail)")
                    self.tableInformationIncomplete.reloadData()
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    //func getDataMethods(){
    func getDataMethods(){
        
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["paymentMethods"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["types"]
                    
                    for i in resultArray.arrayValue {
                        let dato = i.stringValue
                        self.typesPaymentIncomplete.append(dato)
                        
                    }
                    
                    let indexOfA = self.typesPaymentIncomplete.firstIndex(of: self.getData.paymentMethod) // 0
                    if(indexOfA != nil){
                        self.paymentIncompletePicketTextField.text = self.typesPaymentIncomplete[indexOfA!]
                        self.selectedPaymentIncomplete = self.typesPaymentIncomplete[indexOfA!]
                        self.pickerViewPaymentIncomplete.selectRow(indexOfA!, inComponent: 0, animated: true)
                        print(self.typesPaymentIncomplete[indexOfA!])
                    }else{
                        self.paymentIncompletePicketTextField.text = self.typesPaymentIncomplete.first
                        self.selectedPaymentIncomplete = self.typesPaymentIncomplete.first
                    }
                    
                    if(self.selectedPaymentIncomplete == "Efectivo" || self.selectedPaymentIncomplete == "Tarjeta de servicios" || self.selectedPaymentIncomplete == "Cheque" || self.selectedPaymentIncomplete == "Transferencia electrónica de fondos"){
                        self.viewWithFourDigits.isHidden = true
                        self.heightViewFourDigits.constant = 0
                        self.heightWithPaymenteData.constant = 200
                        //heightViewIncomplete.constant = 550
                        self.heightViewPrincipal.constant = 1310
                    }
                    else{
                        self.viewWithFourDigits.isHidden = false
                        self.heightViewFourDigits.constant = 108
                        self.heightWithPaymenteData.constant = 300
                        self.heightViewPrincipal.constant = 1410
                    }
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    func getDataCFDI(){
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["cdfi"] as? [String:Any]
                //print(group_array)
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["types"]
                    
                    for i in resultArray.arrayValue {
                        //print(i)
                        let dato = i.stringValue
                        self.typesUseCFDIIncomplete.append(dato)
                        
                    }
                    
                    let indexOfA = self.typesUseCFDIIncomplete.firstIndex(of: self.getData.cdfi) // 0
                    if(indexOfA != nil){
                        self.useCFDIIncompletePickerTextField.text = self.typesUseCFDIIncomplete[indexOfA!]
                        self.selectedUseCFDIIncomplete = self.typesUseCFDIIncomplete[indexOfA!]
                        self.pickerViewUseCFDIIncomplete.selectRow(indexOfA!, inComponent: 0, animated: true)
                        print(self.typesUseCFDIIncomplete[indexOfA!])
                    }else{
                        self.useCFDIIncompletePickerTextField.text = self.typesUseCFDIIncomplete.first
                        self.selectedUseCFDIIncomplete = self.typesUseCFDIIncomplete.first
                    }
                    
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    func getDataProfilesListIncomplete(){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        
        self.db.collection("users").document(userID).collection("profiles").getDocuments { (snapshot, err) in
            
            
            if let err = err {
                //self.removeSpinner()
                print("Error getting documents: \(err)")
            } else {
                //
                for document in snapshot!.documents {
                    
                    let alias = document.get("alias") as! String
                    self.typeAliasProfileIncomplete.append(alias)
                    
                    let id = document.get("id") as! String
                    self.idProfileIncomplete.append(id)
                    
                }
                
                //self.aliasDataPickerTextField.text = (self.getData.profile["alias"] as! String)
                
                let indexOfA = self.typeAliasProfileIncomplete.firstIndex(of: (self.getData.profile["alias"] as! String))
                let indexOfAId = self.idProfileIncomplete.firstIndex(of: (self.getData.profile["id"] as! String))
                
                /*
                 self.aliasDataPickerTextField.text = self.typeAliasProfileIncomplete.first
                 self.selectedAliasProfileIncomplete = self.typeAliasProfileIncomplete.first
                 self.selectedIdProfileIncomplete = self.idProfileIncomplete.first
                 */
                
                self.aliasDataPickerTextField.text = self.typeAliasProfileIncomplete[indexOfA!]
                self.selectedAliasProfileIncomplete = self.typeAliasProfileIncomplete[indexOfA!]
                self.selectedIdProfileIncomplete = self.idProfileIncomplete[indexOfAId!]
                self.pickerViewAlias.selectRow(indexOfA!, inComponent: 0, animated: true)
                
                self.getRFC(alias: self.selectedAliasProfileIncomplete!)
                self.getDataProfileToInsideInvoices(profileID: self.selectedIdProfileIncomplete!)
                
                
            }
        }
        
    }
    
    func getDataProfileToInsideInvoices(profileID: String){
        let userID = Auth.auth().currentUser!.uid
        
        self.db.collection("users").document(userID).collection("profiles").document(profileID).getDocument { (document, err) in
            
            
            if let document = document, document.exists {
                
                let address = document.get("address") as! String
                let alias = document.get("alias") as! String
                let defaultCdfiProfile = document.get("defaultCdfi") as! Int
                let defaultCompanyIdProfile = document.get("defaultCompanyId") as! String
                let defaultPaymentMethodProfile = document.get("defaultPaymentMethod") as! Int
                let emailProfile = document.get("email") as! String
                let idProfileIncomplet = document.get("id") as! String
                let lastNameFatherProfile = document.get("lastNameFather") as! String
                let lastNameMotherProfile = document.get("lastNameMother") as! String
                let municipalityProfile = document.get("municipality") as! String
                let nameProfileIncomplete = document.get("name") as! String
                let phoneNumberProfile = document.get("phoneNumber") as! String
                let postalCodeProfile = document.get("postalCode") as! String
                let rfcProfileIncomplete = document.get("rfc") as! String
                let imagePositionProfileIncomplete = document.get("selectedImagePosition") as! Int
                let stateProfile = document.get("state") as! String
                let typeProfile = document.get("type") as! Int
                
                let item = ProfileDataModel(address: address, alias: alias, defaultCdfi: defaultCdfiProfile, defaultCompanyId: defaultCompanyIdProfile, defaultPaymentMethod: defaultPaymentMethodProfile, email: emailProfile, idProfile: idProfileIncomplet, lastNameFather: lastNameFatherProfile, lastNameMother: lastNameMotherProfile, municipality: municipalityProfile, name: nameProfileIncomplete, phoneNumber: phoneNumberProfile, postalCode: postalCodeProfile, rfc: rfcProfileIncomplete, imagePosition: imagePositionProfileIncomplete, state: stateProfile, type: typeProfile)
                self.getDataProfile.append(item)
                self.backgroundImageView(data: imagePositionProfileIncomplete)
                
            }
        }
    }
    
    
    func getRFC(alias: String){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        
        //self.showSpinner(onView: self.view)
        self.db.collection("users").document(userID).collection("profiles").getDocuments { (snapshot, err) in
            
            //self.removeSpinner()
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                //self.removeSpinner()
                for document in snapshot!.documents {
                    
                    let aliasData = document.get("alias") as! String
                    if(aliasData == alias){
                        
                        guard let rfcData = document.get("rfc") as? String else {continue}
                        self.rfcIncompleteStatus.text = rfcData
                        
                    }
                    
                }
                
            }
        }
        //self.rfcIncompleteStatus.text
        
    }
    
    func paymentSelectedWithAlias(payment:Int){
        
        self.paymentIncompletePicketTextField.text = self.typesPaymentIncomplete[payment]
        self.selectedPaymentIncomplete = self.typesPaymentIncomplete[payment]
        self.pickerViewPaymentIncomplete.selectRow(payment, inComponent: 0, animated: true)
        print(self.typesPaymentIncomplete[payment])
        
        if(self.selectedPaymentIncomplete == "Efectivo" || self.selectedPaymentIncomplete == "Tarjeta de servicios" || self.selectedPaymentIncomplete == "Cheque" || self.selectedPaymentIncomplete == "Transferencia electrónica de fondos"){
            self.viewWithFourDigits.isHidden = true
            self.heightViewFourDigits.constant = 0
            self.heightWithPaymenteData.constant = 200
            //heightViewIncomplete.constant = 550
            self.heightViewPrincipal.constant = 1310
        }
        else{
            self.viewWithFourDigits.isHidden = false
            self.heightViewFourDigits.constant = 108
            self.heightWithPaymenteData.constant = 300
            self.heightViewPrincipal.constant = 1410
        }
        
    }
    
    func useCDFISelectedWithAlias(cfdiSelected: Int){
        
        self.useCFDIIncompletePickerTextField.text = self.typesUseCFDIIncomplete[cfdiSelected]
        self.selectedUseCFDIIncomplete = self.typesUseCFDIIncomplete[cfdiSelected]
        self.pickerViewUseCFDIIncomplete.selectRow(cfdiSelected, inComponent: 0, animated: true)
        print(self.typesUseCFDIIncomplete[cfdiSelected])
        
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    
}
