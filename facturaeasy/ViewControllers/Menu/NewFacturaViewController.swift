//
//  NewFacturaViewController.swift
//  facturaeasy
//
//  Created by Baxten on 19/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseMLVision
import Purchases
import SwiftyJSON
import GoogleMobileAds


class NewFacturaViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, GADBannerViewDelegate, GADInterstitialDelegate {
    
    @IBOutlet weak var newFacturaCameraButton: UIButton!
    @IBOutlet weak var openTicketGalleryButton: UIButton!
    @IBOutlet weak var suscriptionButton: UIButton!
    @IBOutlet weak var suscriptionLabel: UILabel!
    @IBOutlet weak var suscriptionInactiveLabel: UILabel!
    @IBOutlet weak var scrollViewMainFactura: UIScrollView!
    @IBOutlet weak var viewPrincipal: UIView!
    @IBOutlet weak var paymentButtonInit: UIButton!
    @IBOutlet weak var amountFacturas: UILabel!
    @IBOutlet weak var viewActivePayment: UIView!
    @IBOutlet weak var viewInactivePayment: UIView!
    @IBOutlet weak var viewWithContador: UIView!
    @IBOutlet weak var getMoreInvoicesButton: UIButton!
    @IBOutlet weak var viewWithButtonAndCounter: UIView!
    
    //@IBOutlet var imageView: UIImageView!
    @IBOutlet var chooseBuuton: UIButton!
    var imagePicker = UIImagePickerController()
    var imageView: UIImageView!
    
    lazy var vision = Vision.vision()
    // replace VisionTextDetector with VisionTextRecognizer
    var textRecognizer:  VisionTextRecognizer?
    var allTextFromImage = ""
    var db: Firestore!
    var listProfileRequester = ListProfileRequest()
    var arrayProfiles = [ProfileDataModelInvoices]()
    var companiesName = [String]()
    var arrayTextFromImage = [String]()
    
    var packageAvailable = [Purchases.Package]()
    
    @IBOutlet weak var loadingViewNewFactura: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    var isValid = false
    
    override func viewDidLoad() {
        
        db = Firestore.firestore()
        let vision = Vision.vision()
        textRecognizer = vision.cloudTextRecognizer()
        //textRecognizer = vision.onDeviceTextRecognizer()
        //addObserversValidatePayment()
        addObserversValidatePayment()
        addObserversValidatePaymentAfterPay()
        //self.validPayment()
        paymentButtonInit.showsTouchWhenHighlighted = true
        imagePicker.delegate = self
        
        self.validPaymentSingleton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("entra a viewWillAppear NewFacturaViewController");
        //self.validPaymentApple()
    }
    
    func loadLittleBanner(){
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-app-pub-9763277647634316/9265264708"
        //bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //codigo de prueba
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9763277647634316/1682180350")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        // Gets the domain from which the error came.
        let errorDomain = error.domain
        // Gets the error code. See
        // https://developers.google.com/admob/ios/api/reference/Enums/GADErrorCode
        // for a list of possible codes.
        let errorCode = error.code
        // Gets an error message.
        // For example "Account not approved yet". See
        // https://support.google.com/admob/answer/9905175 for explanations of
        // common errors.
        let errorMessage = error.localizedDescription
        // Gets additional response information about the request. See
        // https://developers.google.com/admob/ios/response-info for more information.
        let responseInfo = error.userInfo[GADErrorUserInfoKeyResponseInfo] as? GADResponseInfo
        // Gets the underlyingError, if available.
        let underlyingError = error.userInfo[NSUnderlyingErrorKey] as? Error
        if let responseInfo = responseInfo {
            print("Received error with domain: \(errorDomain), code: \(errorCode),"
              + "message: \(errorMessage), responseInfo: \(responseInfo),"
              + "underLyingError: \(underlyingError?.localizedDescription ?? "nil")")
        }
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }
    
    func validPaymentApple(){
        Purchases.shared.purchaserInfo { (purchaserInfo, error) in
            if purchaserInfo?.entitlements.all["month"]?.isActive == true {
                // User is "premium"
                print("Suscription active")
            }
            else{
                print("Suscription inactive")
            }
        }
    }
    
    @IBAction func textFromCameraButton(_ sender: UIButton) {
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera)  else {
            let alert = UIAlertController(title: "No camera", message: "This device does not support camera.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        //let picker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraCaptureMode = .photo
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func openTicketButton(_ sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //imageView!.contentMode = .scaleAspectFit
            print("image: \(image)")
            let fileid = dataUser(imageSingleton: image)
            Singleton.shared.setImageTicketGallery(name: fileid)
            runTextRecognizion(with: image)
            
        }
        
        dismiss(animated: true)
        
    }
    
    
    func runTextRecognizion(with image: UIImage){
        let visionImage = VisionImage(image: image)
        
        self.showSpinner(onView: self.view)
        textRecognizer?.process(visionImage) { (features, error ) in
            let text = features
            self.removeSpinner()
            if (text != nil){
                print("text from image: \(text!.text)")
                self.allTextFromImage = text!.text
                
                let text = textImageModel(textImage: self.allTextFromImage)
                Singleton.shared.setTextFromImage(name: text)
                //self.getTextToArray(text: self.allTextFromImage)
                self.getDataProfilesList()
                
                
            }
            else{
                //self.showToast(message: "Por favor ingresa una imagen con texto válida")
                
                let alert = UIAlertController(title: "", message: "Por favor ingresa una imagen con texto válida", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            }
        }
    }
    
    
    func getDataProfilesList(){
        
        listProfileRequester.getListProfileData(){result in
            switch result{
            case .success(let listProfiles):
                self.arrayProfiles = listProfiles
                if (self.arrayProfiles.isEmpty == true){
                    let alert = UIAlertController(title: "", message: "Por favor añade un perfil para poder facturar", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    return
                }
                else{
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListProfilesViewController") as! ListProfilesViewController
                    yourVc.modalPresentationStyle = .overFullScreen
                    self.present(yourVc, animated: true, completion: nil)
                }
                break;
            case .failure(_):
                break;
            }
            
        }
        
    }
    
    func validPaymentSingleton(){
        let isValid = Singleton.shared.getIsValidPayment()?.isValid
        //print("isValid: \(isValid!)")
        let cantidadFacturas = Singleton.shared.getCantidadFacturas()?.cantidad
        amountFacturas.text = String(10 - cantidadFacturas!)
        let saldo = 10 - cantidadFacturas!
        if saldo != 0 {
            
            amountFacturas.isHidden = false
            getMoreInvoicesButton.isHidden = true
            self.viewPrincipal.bringSubviewToFront(self.viewWithContador)
            self.viewPrincipal.sendSubviewToBack(self.viewInactivePayment)
            self.viewPrincipal.sendSubviewToBack(self.viewActivePayment)
            self.view.sendSubviewToBack(self.loadingViewNewFactura)
            self.newFacturaCameraButton.isEnabled = true
            self.openTicketGalleryButton.isEnabled = true
            
            self.loadLittleBanner()
            self.addBannerViewToView(self.bannerView)
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
        }else{
            
            if isValid! == false{
                
                amountFacturas.isHidden = true
                getMoreInvoicesButton.isHidden = false
                self.activityIndicator.stopAnimating()
                self.suscriptionInactiveLabel.text = "No tienes mensualidad activa, presiona aquí para adquirir una mensualidad"
                self.viewPrincipal.bringSubviewToFront(self.viewInactivePayment)
                self.viewPrincipal.sendSubviewToBack(self.viewActivePayment)
                self.view.sendSubviewToBack(self.loadingViewNewFactura)
                self.newFacturaCameraButton.isEnabled = false
                self.openTicketGalleryButton.isEnabled = false
                self.loadLittleBanner()
                self.addBannerViewToView(self.bannerView)
            }else{
                
                self.activityIndicator.stopAnimating()
                self.suscriptionLabel.text = "Tienes tu mensualidad activa"
                self.viewPrincipal.bringSubviewToFront(self.viewActivePayment)
                self.viewPrincipal.sendSubviewToBack(self.viewInactivePayment)
                self.view.sendSubviewToBack(self.loadingViewNewFactura)
                self.newFacturaCameraButton.isEnabled = true
                self.openTicketGalleryButton.isEnabled = true
            }
            
        }
    }
    
    func validPayment(){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        
        let cantidadFacturas = Singleton.shared.getCantidadFacturas()?.cantidad
        amountFacturas.text = String(10 - cantidadFacturas!)
        let saldo = 10 - cantidadFacturas!
        if saldo != 0 {
            
            self.viewPrincipal.bringSubviewToFront(self.viewWithContador)
            self.viewPrincipal.sendSubviewToBack(self.viewInactivePayment)
            self.viewPrincipal.sendSubviewToBack(self.viewActivePayment)
            self.view.sendSubviewToBack(self.loadingViewNewFactura)
            self.newFacturaCameraButton.isEnabled = true
            self.openTicketGalleryButton.isEnabled = true
            
            self.loadLittleBanner()
            self.addBannerViewToView(self.bannerView)
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
        }else{
            print("Cantidad facturas newFactura view: \(cantidadFacturas!)")
            
            let docRef = db.collection("validateMonthlyPaymentByUser").document(userID)
            
            docRef.getDocument { (document, error) in
                
                if let document = document, document.exists {
                    
                    let serverTimestamp = document.get("timeStampEnd") as! String
                    let date = NSDate(timeIntervalSince1970: Double(serverTimestamp)!)
                    let dateTod = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"
                    let dateToday2 = formatter.string(from: dateTod)
                    let dateServer2 = formatter.string(from: date as Date)
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                    dateFormatter2.dateFormat = "dd-MM-yyyy"
                    let dateToday = dateFormatter2.date(from:dateToday2)
                    let dateServer = dateFormatter2.date(from:dateServer2)
                    
                    print("dateToday: \(dateToday!)")
                    print("dateServer: \(dateServer!)")
                    
                    if(dateServer! <= dateToday!){
                        
                        self.activityIndicator.stopAnimating()
                        self.isValid = false
                        print("Tu suscripción ha vencido")
                        
                        let isVal = isValidPayment(isValid: self.isValid)
                        Singleton.shared.setIsValidPayment(name: isVal)
                        self.suscriptionInactiveLabel.text = "No tienes mensualidad activa, presiona aquí para adquirir una mensualidad"
                        self.viewPrincipal.bringSubviewToFront(self.viewInactivePayment)
                        self.viewPrincipal.sendSubviewToBack(self.viewActivePayment)
                        self.view.sendSubviewToBack(self.loadingViewNewFactura)
                        self.newFacturaCameraButton.isEnabled = false
                        self.openTicketGalleryButton.isEnabled = false
                        
                        self.loadLittleBanner()
                        self.addBannerViewToView(self.bannerView)
                        
                    }else{
                        self.activityIndicator.stopAnimating()
                        //self.removeSpinner()
                        self.isValid = true
                        print("Tu suscripción sigue vigente")
                        let isVal = isValidPayment(isValid: self.isValid)
                        Singleton.shared.setIsValidPayment(name: isVal)
                        //self.validatePaymentObserver(isValidPayment: isValid)
                        self.suscriptionLabel.text = "Tienes tu mensualidad activa"
                        
                        //self.scrollViewMainFactura.bringSubviewToFront(self.viewActivePayment)
                        //self.scrollViewMainFactura.sendSubviewToBack(self.viewInactivePayment)
                        self.viewPrincipal.bringSubviewToFront(self.viewActivePayment)
                        self.viewPrincipal.sendSubviewToBack(self.viewInactivePayment)
                        self.view.sendSubviewToBack(self.loadingViewNewFactura)
                        
                        self.newFacturaCameraButton.isEnabled = true
                        self.openTicketGalleryButton.isEnabled = true
                    }
                    
                    
                    
                } else {
                    self.activityIndicator.stopAnimating()
                    //isValid = false
                    print("Tu suscripción ha vencido")
                    
                    //let isVal = isValidPayment(isValid: isValid)
                    //Singleton.shared.setIsValidPayment(name: isVal)
                    //self.validatePaymentObserver(isValidPayment: isValid)
                    self.suscriptionInactiveLabel.text = "No tienes mensualidad activa, presiona aquí para adquirir una mensualidad"
                    
                    //self.scrollViewMainFactura.bringSubviewToFront(self.viewInactivePayment)
                    //self.scrollViewMainFactura.sendSubviewToBack(self.viewActivePayment)
                    self.viewPrincipal.bringSubviewToFront(self.viewInactivePayment)
                    self.viewPrincipal.sendSubviewToBack(self.viewActivePayment)
                    self.view.sendSubviewToBack(self.loadingViewNewFactura)
                    
                    self.newFacturaCameraButton.isEnabled = false
                    self.openTicketGalleryButton.isEnabled = false
                    //self.removeSpinner()
                    print("Document does not exist")
                    
                    self.loadLittleBanner()
                    self.addBannerViewToView(self.bannerView)
                }
            }
        }
        
    }
    
    
    @IBAction func paymentButton(_ sender: UIButton) {
        
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
        //yourVc.modalPresentationStyle = .overFullScreen
        self.present(yourVc, animated: true, completion: nil)
        
    }
    
    @IBAction func suscriptipnAction(_ sender: UIButton) {
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
        //yourVc.modalPresentationStyle = .overFullScreen
        self.present(yourVc, animated: true, completion: nil)
    }
    
}

extension NewFacturaViewController {
    
    func addObserversValidatePayment() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceivedValidationPayment), name: .validPayment, object: nil)
    }
    
    
    @objc func notificationReceivedValidationPayment(_ notification: Notification) {
        
        self.activityIndicator.startAnimating()
        self.view.bringSubviewToFront(self.loadingViewNewFactura)
        self.validPaymentSingleton()
        
        
    }
    
    
    func addObserversValidatePaymentAfterPay() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceivedValidationPayment), name: .validPaymentAfterPay, object: nil)
    }
    
    
    @objc func notificationReceivedValidationPaymentAfterPay(_ notification: Notification) {
        
        self.activityIndicator.startAnimating()
        self.view.bringSubviewToFront(self.loadingViewNewFactura)
        self.validPaymentSingleton()
        
        
    }
    
}


extension NewFacturaViewController {
    
    
}

