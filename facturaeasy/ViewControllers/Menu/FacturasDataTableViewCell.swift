//
//  FacturasDataTableViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 05/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

class FacturasDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageStatusFactura: UIImageView!
    @IBOutlet weak var companyFacturaLabel: UILabel!
    @IBOutlet weak var amountFacturaLabel: UILabel!
    @IBOutlet weak var aliasFacturaLabel: UILabel!
    @IBOutlet weak var dateFacturaLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
