//
//  ProcessTicketViewController.swift
//  facturaeasy
//
//  Created by Baxten on 18/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON
import FirebaseMLVision

enum PaymentType {
    case Efectivo, Tarjeta_de_servicios, Cheque, Transferencia_electrónica_de_fondos
    
    func description() -> String {
        switch self {
        case .Efectivo:
            return "Efectivo"
        case .Tarjeta_de_servicios:
            return "Tarjeta de servicios"
        case .Cheque:
            return "Cheque"
        case .Transferencia_electrónica_de_fondos:
            return "Transferencia electrónica de fondos"
        }
    }
}

class ValuesInformation: Codable{
    var key: String = ""
    var type: String = ""
    var value: String?
    var getNextLine: Bool?
    var name: String = ""
    var separator: String = ""
    
    init(key: String, type: String,  value: String, getNextLine: Bool, name: String, separator: String) {
        self.key = key
        self.type = type
        self.value = value
        self.getNextLine = getNextLine
        self.name = name
        self.separator = separator
    }
}



extension Notification.Name {
    
    static let addNewFactura = Notification.Name(
        rawValue: "NewFacturaAdded")
    
}


class ProcessTicketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UINavigationControllerDelegate {
    
    
    var validation = Validation()
    var typesCount: Int = 0;
    @IBOutlet weak var hideLabelProcess: UILabel!
    
    @IBOutlet weak var hightTicketProcess: NSLayoutConstraint!
    
    var iconClick = true
    
    @IBOutlet weak var imageTicketProcess: UIImageView!
    @IBOutlet weak var aliasProcessLabel: UILabel!
    @IBOutlet weak var rfcProcessLabel: UILabel!
    @IBOutlet weak var fourLastDigits: UITextField!
    @IBOutlet var backgroundImageProcess: UIView!
    @IBOutlet weak var tableViewProcessTicket: UITableView!
    @IBOutlet weak var companyDataPickerTextField: UITextField!
    @IBOutlet weak var useCFDIDataPicketTextField: UITextField!
    @IBOutlet weak var paymentDataPicketTextField: UITextField!
    @IBOutlet weak var heigthScrollviewPrincipal: NSLayoutConstraint!
    @IBOutlet weak var heightFourDigits: NSLayoutConstraint!
    @IBOutlet weak var viewWithFourDigits: UIView!
    @IBOutlet weak var heightViewPrincipal: NSLayoutConstraint!
    @IBOutlet weak var heightViewCFDI: NSLayoutConstraint!
    
    
    var pickerViewCompanies = UIPickerView()
    var pickerViewPayment = UIPickerView()
    var pickerViewUseCFDI = UIPickerView()
    
    var typesCompaniesProcess = [String]()
    var typesPaymentProcess = Singleton.shared.getPayment()?.payment
    var typesUseCFDIProcess = Singleton.shared.getCFDI()?.cfdi
    
    var rowSelectedProcess: String = ""
    var selectedCompaniesProcess: String?
    var selectedUseCFDIProcess: String?
    var selectedPaymentProcess: String?
    
    var selectedTypeRow: Int = 0
    var selectedMethodRow: Int = 0
    var selectedCFDIRow: Int = 0
    
    var imageProcess: Int = 0
  
    
    var getDataProfile: ProfileDataModelInvoices!
    var db: Firestore!
    
    var numOfRow = 1
    var tagData = ""
    var valuesData = [JSON]()
    var valuesInformationArray = [ValuesInformation]()
    
    
    var invoicesValues = ""
    
    var dato1TextField = ""
    var dato2TextField = ""
    var dato3TextField = ""
    var dato4TextField = ""
    var dato5TextField = ""
    var dato6TextField = ""
    var dato7TextField = ""
    
    var amount = ""
    var valoresArray = [String]()
    
    override func viewDidLoad() {
        
        db = Firestore.firestore()
        self.tableViewProcessTicket.delegate = self
        self.tableViewProcessTicket.dataSource = self
        self.tableViewProcessTicket.rowHeight = 44;
        self.dismissKey()
        self.tableViewProcessTicket.separatorStyle = .none
        //let imageSingleto = Singleton.shared.getImageTicketGallery()?.imageSingleton
        
        let image = Singleton.shared.getImageTicketGallery()?.imageSingleton
        imageTicketProcess.image = image
        getDataInfo()
        getDataCompaniesNameProcess()
        createPickerView()
        dismissPickerView()
        self.getDataMethods(position: getDataProfile.defaultPaymentMethod)
        self.getDataCFDI(position: getDataProfile.defaultCdfi)
        iconsTextField()
        print("entra a ProcessTicketViewController")
        imageTicketProcess.contentMode = .scaleAspectFit
        
    }
    
    func iconsTextField(){
        
        paymentDataPicketTextField.rightViewMode = UITextField.ViewMode.always
        paymentDataPicketTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        useCFDIDataPicketTextField.rightViewMode = UITextField.ViewMode.always
        useCFDIDataPicketTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
        companyDataPickerTextField.rightViewMode = UITextField.ViewMode.always
        companyDataPickerTextField.rightView = UIImageView(image: UIImage(named: "ic_arrow_26"))
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.pickerViewCompanies {
            typesCount = typesCompaniesProcess.count
        }
        
        if pickerView == self.pickerViewUseCFDI {
            typesCount = typesUseCFDIProcess!.count
        }
        
        if pickerView == self.pickerViewPayment {
            typesCount = typesPaymentProcess!.count
        }
        
        return typesCount
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.pickerViewCompanies {
            rowSelectedProcess = typesCompaniesProcess[row]
        }
        
        if pickerView == self.pickerViewUseCFDI {
            rowSelectedProcess = typesUseCFDIProcess![row]
        }
        
        if pickerView == self.pickerViewPayment {
            rowSelectedProcess = typesPaymentProcess![row]
        }
        
        return rowSelectedProcess
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerViewCompanies {
            selectedCompaniesProcess = typesCompaniesProcess[row] // selected item
            companyDataPickerTextField.text = selectedCompaniesProcess
            print("selectedType: \(selectedCompaniesProcess!)")
            self.getInformationCompanies(name: selectedCompaniesProcess!)
        }
        
        if pickerView == pickerViewUseCFDI {
            selectedCFDIRow = row
            selectedUseCFDIProcess = typesUseCFDIProcess![row] // selected item
            useCFDIDataPicketTextField.text = selectedUseCFDIProcess
            print("selectedType: \(selectedUseCFDIProcess!)")
            
        }
        
        if pickerView == pickerViewPayment {
            selectedMethodRow = row
            selectedPaymentProcess = typesPaymentProcess![row] // selected item
            paymentDataPicketTextField.text = selectedPaymentProcess
            print("selectedType: \(selectedPaymentProcess!)")
            if(selectedPaymentProcess == PaymentType.Efectivo.description() || selectedPaymentProcess == PaymentType.Tarjeta_de_servicios.description() || selectedPaymentProcess == PaymentType.Cheque.description() || selectedPaymentProcess == PaymentType.Transferencia_electrónica_de_fondos.description()){
                viewWithFourDigits.isHidden = true
                heightFourDigits.constant = 0
                heightViewCFDI.constant = 200
                heightViewPrincipal.constant = 1326
                heigthScrollviewPrincipal.constant = 1326
            }
            else{
                viewWithFourDigits.isHidden = false
                heightFourDigits.constant = 108
                heightViewCFDI.constant = 300
                heightViewPrincipal.constant = 1426
                heigthScrollviewPrincipal.constant = 1426
            }
            
        }
        
    }
    
    func createPickerView() {
        
        pickerViewCompanies.delegate = self
        companyDataPickerTextField.inputView = pickerViewCompanies
        
        pickerViewUseCFDI.delegate = self
        useCFDIDataPicketTextField.inputView = pickerViewUseCFDI
        
        pickerViewPayment.delegate = self
        paymentDataPicketTextField.inputView = pickerViewPayment
        
    }
    
    func dismissPickerView() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        companyDataPickerTextField.inputAccessoryView = toolBar
        
        let toolBar2 = UIToolbar()
        toolBar2.sizeToFit()
        let button2 = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar2.setItems([button2], animated: true)
        toolBar2.isUserInteractionEnabled = true
        useCFDIDataPicketTextField.inputAccessoryView = toolBar2
        
        let toolBar3 = UIToolbar()
        toolBar3.sizeToFit()
        let button3 = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(self.action))
        toolBar3.setItems([button3], animated: true)
        toolBar3.isUserInteractionEnabled = true
        paymentDataPicketTextField.inputAccessoryView = toolBar3
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    func getDataInfo(){
        backgroundImageView(data: imageProcess)
        aliasProcessLabel.text = getDataProfile.alias
        rfcProcessLabel.text = getDataProfile.rfc
    }
    
    func backgroundImageView(data: Int){
        //let data = indexPath.item
        print("data position image: \(data)")
        switch data {
        case 0:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
        case 1:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_city_background")!)
        case 2:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_mountain_background")!)
        case 3:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_pyramid_background")!)
        case 4:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_snow_background")!)
        case 5:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_space_background")!)
            
        default:
            self.backgroundImageProcess.backgroundColor = UIColor(patternImage: UIImage(named: "ic_profile_beach_background")!)
            print("Have you done something new?")
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        valuesInformationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
        let item = valuesInformationArray[indexPath.row]
        cell.nameProcessLabel.text = item.key
        //cell.valueTicketTextField.text = item.value
        switch(indexPath.row)
        {
        case 0 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 0
            self.tagData = cell.valueTicketTextField.text!
            break;
            
        case 1 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 1
            break;
            
        case 2 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 2
            break;
            
        case 3 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 3
            break;
        case 4 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 4
            break;
        case 5 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 5
            break;
        case 6 :
            cell = tableView.dequeueReusableCell(withIdentifier: "ProcessTicketsTableViewCell", for: indexPath) as! ProcessTicketsTableViewCell
            cell.configure(lblValue: item.key, txtValue: item.value!)
            cell.valueTicketTextField.tag = 6
            break;
            
        default :
            break;
            
        }
        
        editChange(cell.valueTicketTextField)
        return cell
        
    }
    
    @IBAction func editChange(_ sender: UITextField) {
        if(sender.text == ""){
            return
        }
        let pos = sender.tag
        
        let item = self.valuesInformationArray[pos]
        item.value = sender.text!
        
        if(item.key == "Monto" || item.key == "Total"){
            self.amount = item.value!
            print("amount: \(self.amount)")
            //self.amount =
        }
        
        if(item.key == ""){
            return
        }
        //print("self.myresult2: \(self.myresult2!)")
        print("pos: \(item)")
        print("item.value: \(item.value!)")
        
    }
    
    
    func getDataCompaniesNameProcess(){
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["companies"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["companiesList"]
                    
                    for i in resultArray.arrayValue {
                        
                        let createAt = i["name"].stringValue
                        self.typesCompaniesProcess.append(createAt)
                        
                    }
                    
                    self.companyDataPickerTextField.text = self.typesCompaniesProcess.first
                    self.selectedCompaniesProcess = self.typesCompaniesProcess.first
                    self.getInformationCompanies(name: self.selectedCompaniesProcess!)
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    
    func getInformationCompanies(name: String){
        
        self.valoresArray.removeAll()
        self.valuesData.removeAll()
        self.valuesInformationArray.removeAll()
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["companies"] as? [String:Any]
                //print(group_array) 
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["companiesList"]
                    //let history = resultArray["values"]
                    
                    for i in resultArray.arrayValue {
                        
                        let nameService = i["name"].stringValue
                        if(nameService == name){
                            print("name: \(name)")
                            print("entra if nameService == name")
                            let valores = i["values"].arrayValue
                            //print("valores: \(valores)")
                            self.valuesData.append(contentsOf: valores)
                            for i in self.valuesData {
                                //print(i)
                                
                                let displayName = i["displayName"].stringValue
                                let getNextLine = i["getNextLine"].bool
                                let name = i["name"].stringValue
                                let separator = i["separator"].stringValue
                                let type = i["type"].stringValue
                                
                                let item = ValuesInformation(key: displayName, type: type, value: "", getNextLine: getNextLine!, name: name, separator: separator)
                                self.valuesInformationArray.append(item)
                                
                            }
                            self.getValue(values: valores)
                            
                        }
                        
                    }
                    
                    self.tableViewProcessTicket.reloadData()
                    
                } catch
                {
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    func getValue(values: [JSON]){
        
        
        let datoImagen = Singleton.shared.getTextFromImage()?.textImage
        let valores = values.count
       
        for element in self.valuesInformationArray  {
            
            
            if let range = datoImagen!.range(of: element.name) {
                
                if element.getNextLine == false {
                    var stringValue = ""
                    let getValue = datoImagen![range.upperBound...].trimmingCharacters(in: .newlines)
                    print("getValue: \(getValue)") // prints "123.456.7891"
                    
                    if element.type == "amount"{
                        stringValue = getValue.replacingOccurrences(of: "$", with: "")
                    }else{
                        stringValue = getValue
                    }
                    
                    let lines = stringValue.components(separatedBy: "\n")
                    valoresArray.append(lines[0])
                    
                    
                }
                
                if element.getNextLine == true {
                    var stringValue = ""
                    let getValue = datoImagen![range.upperBound...].trimmingCharacters(in: .newlines)
                    print("getValue: \(getValue)") // prints "123.456.7891"
                    
                    if element.type == "amount"{
                        stringValue = getValue.replacingOccurrences(of: "$", with: "")
                    }else{
                        stringValue = getValue
                    }
                    
                    let lines = stringValue.components(separatedBy: "\n")
                    valoresArray.append(lines[0])
                    
                }
                
            }else{
                
                if let range = datoImagen!.range(of: element.name.uppercased()) {
                    
                    if element.getNextLine == false {
                        var stringValue = ""
                        let getValue = datoImagen![range.upperBound...].trimmingCharacters(in: .whitespaces)
                        print("getValue: \(getValue)") // prints "123.456.7891"
                        
                        if element.type == "amount"{
                            stringValue = getValue.replacingOccurrences(of: "$", with: "")
                        }else{
                            stringValue = getValue
                        }
                        
                        let lines = stringValue.components(separatedBy: "\n")
                        valoresArray.append(lines[0])
                        
                    }
                    
                    if element.getNextLine == true {
                        var stringValue = ""
                        let getValue = datoImagen![range.upperBound...].trimmingCharacters(in: .newlines)
                        print("getValue: \(getValue)") // prints "123.456.7891"
                        
                        if element.type == "amount"{
                            stringValue = getValue.replacingOccurrences(of: "$", with: "")
                        }else{
                            stringValue = getValue
                        }
                        
                        let lines = stringValue.components(separatedBy: "\n")
                        valoresArray.append(lines[0])
                        
                    }
                    
                }else{
                    valoresArray.append("")
                }
                
            }
            
        }
        
        print("valoresArray: \(self.valoresArray)")
        print("tamaño valuesInformationArray[i]: \(self.valuesInformationArray.count)")
        print("tamaño valoresArray[i]: \(self.valoresArray.count)")
        
        if self.valuesInformationArray.count != 0 && self.valoresArray.count != 0{
            if self.valuesInformationArray.count ==  self.valoresArray.count{
                for i in 0...valores - 1{
                    
                    print("self.valuesInformationArray[i]: \(self.valuesInformationArray[i].key)")
                    print("self.valoresArray[i]: \(self.valoresArray[i])")
                    self.valuesInformationArray[i].value = self.valoresArray[i]
                    print("self.valuesInformationArray[i].value: \(self.valuesInformationArray[i].value!)")
                    //print("item.value: \(self.valoresArray[i])")
                }
            }
        }
        
        
        for element in self.valuesInformationArray  {
            print("elements: \(element.key)")
            print("elements: \(element.value!)")
        }
        
        
        self.tableViewProcessTicket.reloadData()
        
    }
    
    func getDataMethods(position: Int){
        
        self.paymentDataPicketTextField.text = self.typesPaymentProcess![position]
        self.pickerViewPayment.selectRow(position, inComponent: 0, animated: true)
        self.selectedPaymentProcess = self.typesPaymentProcess![position]
        print(self.typesPaymentProcess![position])
        
        print("self.selectedPaymentProcess: \(self.selectedPaymentProcess!)")
        print("PaymentType.Efectivo.description(): \(PaymentType.Efectivo.description())")
        
        if(self.selectedPaymentProcess == PaymentType.Efectivo.description() || self.selectedPaymentProcess == PaymentType.Tarjeta_de_servicios.description() || self.selectedPaymentProcess == PaymentType.Cheque.description() || self.selectedPaymentProcess == PaymentType.Transferencia_electrónica_de_fondos.description()){
            self.viewWithFourDigits.isHidden = true
            self.heightFourDigits.constant = 0
            self.heightViewCFDI.constant = 200
            self.heightViewPrincipal.constant = 1326
            self.heigthScrollviewPrincipal.constant = 1326
        }
        else{
            self.viewWithFourDigits.isHidden = false
            self.heightFourDigits.constant = 108
            self.heightViewCFDI.constant = 300
            self.heightViewPrincipal.constant = 1426
            self.heigthScrollviewPrincipal.constant = 1426
        }
        
    }
    
    func getDataCFDI(position: Int){
        
        self.useCFDIDataPicketTextField.text = self.typesUseCFDIProcess![position]
        self.pickerViewUseCFDI.selectRow(position, inComponent: 0, animated: true)
        self.selectedUseCFDIProcess = self.typesUseCFDIProcess![position]
        print(self.typesUseCFDIProcess![position])
        
    }
    
    
    @IBAction func saveTicketsProcessButton(_ sender: UIButton) {
        
        let company = self.selectedCompaniesProcess
        print("company: \(company!)")
        let fourDigits = fourLastDigits.text
        print("fourDigits: \(fourDigits!)")
        let cfdi = self.selectedUseCFDIProcess
        print("cfdi: \(cfdi!)")
        let payment = self.selectedPaymentProcess
        print("payment: \(payment!)")
        
        let imagen = Singleton.shared.getImageTicketGallery()?.imageSingleton
        let imageCompress = imagen!.compress(to: 400)
        let imageString = UIImage(data: imageCompress)
        let imageStringConvert = self.convertImageToBase64(image: imageString!)
        
        //print("imageString: \(imageString)")
        guard let retrievedText = Singleton.shared.getTextFromImage()?.textImage else { return }
        print("retrievedText: \(retrievedText)")
        let userID = Auth.auth().currentUser!.uid
        let ref = db.collection("users")
        let docId = ref.document().documentID
        
        var status = ""
        
        if(payment == PaymentType.Efectivo.description() || payment == PaymentType.Tarjeta_de_servicios.description() || payment == PaymentType.Cheque.description() || payment == PaymentType.Transferencia_electrónica_de_fondos.description()){
            status = "PENDING"
        }
        else{
            if (fourDigits == ""){
                status = "INCOMPLETE"
            }else{
                let isValidFour = self.validation.validateFourDigitsCard(four: fourDigits!)
                if (isValidFour == false) {
                    print("Por favor ingresa los 4 últimos dígitos de tu tarjeta correctamente")
                    showToast(message: "Por favor ingresa los 4 últimos dígitos de tu tarjeta")
                    return
                }
                status = "PENDING"
            }
        }
      
        
        for i in 0...self.valuesInformationArray.count - 1{
            
            let j = self.valuesInformationArray[i]
            print("value information validation: \(j.value!)")
            if (j.value == ""){
                showToast(message: "Por favor ingresa la información requerida por la empresa")
                return
            }
            else{
                continue
            }
            
        }
        
        do {
            
            let encodedDictionary = try JSONEncoder().encode(self.valuesInformationArray)
            print("encodedDictionary: \(encodedDictionary)")
            let result = try? JSON(data: encodedDictionary)
            print("myresult2: \(result!)")
            let str = String(decoding: encodedDictionary, as: UTF8.self)
            print("str: \(str)")
            
            self.invoicesValues = str
            
            
        } catch {
            print("Error: ", error)
        }
        
        
        let docData: [String: Any] = [
            
            "amount": self.amount,
            "cardNumber": fourDigits!,
            "cdfi": cfdi!,
            "company": company!,
            "date": "",
            "id": docId,
            "invoiceValues": self.invoicesValues,
            "paymentMethod": payment!,
            "retrievedText": retrievedText,
            "serverTimestamp": Timestamp(date: Date()),
            "status": status ,
            "userId": userID,
            "profile": [
                "address": getDataProfile.address,
                "alias": getDataProfile.alias,
                "defaultCdfi": getDataProfile.defaultCdfi,
                "defaultCompanyId": getDataProfile.defaultCompanyId,
                "defaultPaymentMethod": getDataProfile.defaultPaymentMethod,
                "email": getDataProfile.email,
                "enabled": true,
                "id": getDataProfile.idProfile,
                "lastNameFather": getDataProfile.lastNameFather,
                "lastNameMother": getDataProfile.lastNameMother,
                "municipality": getDataProfile.municipality,
                "name": getDataProfile.name,
                "phoneNumber": getDataProfile.phoneNumber,
                "postalCode": getDataProfile.postalCode,
                "rfc": getDataProfile.rfc,
                "selectedImagePosition": getDataProfile.imagePosition,
                "state": getDataProfile.state,
                "type": getDataProfile.type
            ]
        ]
        
        let docDataImage: [String: Any] = [
            
            "image": imageStringConvert,
            "invoiceId": docId
            
        ]
        
        db.collection("users").document(userID).collection("invoices").document(docId).setData(docData) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                
                self.sendAddedNewPFacturaObserver()
                print("Document successfully written!")
                print("docData: \(docData)")
                
            }
        }
        
        db.collection("users").document(userID).collection("invoice_images").document(docId).setData(docDataImage) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("image successfully written!")
            }
        }
        
    }
    
    func addInvoicesToPendings(docID: String){
        
        //let userID = Auth.auth().currentUser!.uid
        let docData = ["ruta": "invoices/\(docID)"]
        
        db.collection("pendings").document(docID).setData(docData) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                
                print("Document successfully written in pendings!")
                
            }
        }
    }
    
    private func sendAddedNewPFacturaObserver(){
        NotificationCenter.default.post(name: .addNewFactura, object: nil)
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
    }
    
    
    @IBAction func showImageProcess(_ sender: UIButton) {
        let ver = "Ver"
        let ocultar = "Ocultar"
        
        if(iconClick == true) {
            hightTicketProcess.constant = 35
            heigthScrollviewPrincipal.constant = 900
            
            imageTicketProcess.isHidden = true
            hideLabelProcess.text = ver;
            
        } else {
            hightTicketProcess.constant = 550
            heigthScrollviewPrincipal.constant = 1473
            
            imageTicketProcess.isHidden = false
            hideLabelProcess.text = ocultar;
        }
        iconClick = !iconClick
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        //let imagen = imageData.jpegData(compressionQuality: 0.75)
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}

extension UIImage {
    func toString() -> String? {
        let data: Data? = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}

extension UIImage {
    func toStringPNG() -> String? {
        
        let pngData = self.pngData()
        return pngData?.base64EncodedString(options: .lineLength64Characters)
    }
    
}







