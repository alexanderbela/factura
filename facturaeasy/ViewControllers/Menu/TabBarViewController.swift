

import UIKit
import Firebase

extension Notification.Name {
    
    static let addNewFacturaRefresh = Notification.Name(
        rawValue: "NewFacturaAddedRefresh")
    
    static let profileRefresh = Notification.Name(
        rawValue: "profileRefresh")
    
    static let validPayment = Notification.Name(
        rawValue: "validPayment")
    
}

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    
    var db: Firestore!
    override func viewDidLoad() {
        super.viewDidLoad()
        createTab()
        db = Firestore.firestore()
        self.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func createTab(){
        // Create Tab one
        let tabOne = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let tabOneBarItem = UITabBarItem(title: "Inicio", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
        tabOne.tabBarItem = tabOneBarItem
        
        
        let tabTwo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewFacturaViewController") as! NewFacturaViewController
        let tabTwoBarItem2 = UITabBarItem(title: "Nueva", image: UIImage(named: "icons8-añadir-24"), selectedImage: UIImage(named: "icons8-añadir-24"))
        tabTwo.tabBarItem = tabTwoBarItem2
        
        let tabThree = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileListDataViewController") as! ProfileListDataViewController
        let tabThreeBarItem3 = UITabBarItem(title: "Perfiles", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
        tabThree.tabBarItem = tabThreeBarItem3
        
        
        self.viewControllers = [tabOne, tabTwo,tabThree]
    }
    
    // UITabBarControllerDelegate method
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //print("Selected \(viewController.title!)")
        
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            //do your stuff
            print("tabBar selected: 0")
            self.sendAddedNewPFacturaObserver()
            //self.validPayment()
        }
        if tabBarIndex == 1 {
            //do your stuff
            print("tabBar selected: 1")
            //self.validPayment()
        }
        if tabBarIndex == 2 {
            //do your stuff
            print("tabBar selected: 2")
            //self.profileRefreshObserver()
            //self.validPayment()
        }
        
    }
    
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
    }

    // UITabBarControllerDelegate
    private func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        print("Selected view controller")
        //let tabBarIndex = tabBarController.selectedIndex
        //if tabBarIndex == 0 {
            //do your stuff
        //}
    }
    
    private func sendAddedNewPFacturaObserver(){
        NotificationCenter.default.post(name: .addNewFacturaRefresh, object: nil)
        
    }
    
    private func profileRefreshObserver(){
        NotificationCenter.default.post(name: .profileRefresh, object: nil)
        
    }
    
    private func validPayment(){
        NotificationCenter.default.post(name: .validPayment, object: nil)
        
    }
}
