//
//  ProcessTicketsTableViewCell.swift
//  facturaeasy
//
//  Created by Baxten on 19/08/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit
import Foundation



class ProcessTicketsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameProcessLabel: UILabel!
    @IBOutlet weak var valueTicketTextField: UITextField!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(lblValue : String,txtValue :String){
        
        nameProcessLabel.text = lblValue
        valueTicketTextField.text = txtValue
        
        
        
    }
    
    
    
    
}
