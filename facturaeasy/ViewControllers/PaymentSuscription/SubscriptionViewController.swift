//
//  SubscriptionViewController.swift
//  facturaeasy
//
//  Created by Baxten on 19/10/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Purchases
import Firebase

extension Notification.Name {
    
    static let validPaymentAfterPay = Notification.Name(
        rawValue: "validPaymentAfterPay")
    
}


class SubscriptionViewController: UIViewController {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var paymentButton: UIButton!
    
    var packageAvailable = Purchases.Package()
    //let pack
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.paymentValidation()
    }
    
    
    func paymentValidation(){
        
        Purchases.shared.offerings { (offerings, error) in
            if let offerings = offerings {
                //showPaywall(offerings.current)
                
                let offer = offerings.current
                let packages = offer?.availablePackages
                //print("offerings.current: \(offerings.current!)")
                
                guard packages != nil else { return }
                
                for i in 0...packages!.count - 1{
                    //get a reference of package
                    let package = packages![i]
                    
                    self.packageAvailable = package
                    //get a reference of product
                    let product = package.product
                    //product title
                    let title = product.localizedTitle
                    //product price
                    let price = product.price
                    //product duration
                    var duration = ""
                    let subscriptionPeriod = product.subscriptionPeriod
                    
                    switch subscriptionPeriod!.unit {
                    case SKProduct.PeriodUnit.month:
                        duration = "\(subscriptionPeriod!.numberOfUnits) Month"
                    default:
                        duration = ""
                    }
                    
                    //self.titleLabel.text = title
                    //self.priceLabel.text = String(price)
                    //self.subTitleLabel.text = duration
                }
                
            }
        }
    }
    
    @IBAction func paymentAction(_ sender: UIButton) {
        
        let currentDate = Date()
        let fechaInicial = Timestamp(date: currentDate)
        //print("fecha timeStapm: \(fechaInicial.seconds)")
        
        let stringFecha = String(fechaInicial.seconds)
        //print("fecha stringFecha: \(stringFecha)")
        
        var dateComponent = DateComponents()
        dateComponent.month = 1
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        let fechaFuture = Timestamp(date: futureDate!)
        let fechaFinall = String(fechaFuture.seconds)
        
        print("currentDate: \(currentDate)")
        print("futureDate: \(futureDate!)")
        print("Fecha inicial timestamp: \(stringFecha)")
        print("Fecha final timestamp: \(fechaFinall)")
        
        
        let userID = Auth.auth().currentUser!.uid
        let package = self.packageAvailable
        
        Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, userCancelled) in
            if purchaserInfo?.entitlements.all["month"]?.isActive == true {
                // Unlock that great "pro" content
                
                let newUserReference = Firestore.firestore().collection("validateMonthlyPaymentByUser").document(userID)    // <-- create a document, with the user id from Firebase Auth
                
                newUserReference.setData([
                    "timeStampEnd": fechaFinall,
                    "timeStampStart": stringFecha,
                    "userId": userID
                ])
                
                let item = isValidPayment(isValid: true)
                Singleton.shared.setIsValidPayment(name: item)
                self.validPayment()
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    private func validPayment(){
        NotificationCenter.default.post(name: .validPaymentAfterPay, object: nil)
        NotificationCenter.default.post(name: .validPayment, object: nil)
        
    }
    
}
