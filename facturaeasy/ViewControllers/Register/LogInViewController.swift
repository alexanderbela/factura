//
//  LogInViewController.swift
//  facturaeasy
//
//  Created by Baxten on 21/07/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension Notification.Name {
    static let validatePaymentByUser = Notification.Name(
        rawValue: "validatePayment")
    
}


class LogInViewController: UIViewController {
    
    
    var db: Firestore!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var showEye: UIButton!
    @IBOutlet weak var warningEmailLabel: UILabel!
    @IBOutlet weak var warningPasswordLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var imageViewBackground: UIImageView!
    
    
    var validation = Validation()
    var iconClick = true
    var arrayProfiles = [ProfileDataModelInvoices]()
    var listProfileRequester = ListProfileRequest()
    
    override func viewDidLoad() {
        
        db = Firestore.firestore()
        passwordTextField.isSecureTextEntry = true
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        imageViewBackground?.backgroundColor = UIColor(white: 0.5, alpha: 0.4)
        
        
    }
    
    
    @IBAction func logInButtonAction(_ sender: UIButton) {
        
        let email = emailTextField.text
        let password = passwordTextField.text
        
        
        
        login(withEmail: email!, password: password!)
    }
    
    func login(withEmail email: String, password: String, _ callback: ((Error?) -> ())? = nil){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let e = error{
                callback?(e)
                print("error login", e)
                let alert = UIAlertController(title: "", message: "Usuario o contraseña no validos", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                //alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
                return
            }
            
            print("login completado")
            self.openHome()
            //self.validPayment()
            callback?(nil)
        }
    }
    
    func openHome(){
        //self.validPayment()
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        
        getDataProfilesList()
        
    }
    
    @IBAction func showEyeAction(_ sender: UIButton) {
        let imageOn = UIImage(named: "passwordon") as UIImage?
        let imageOff = UIImage(named: "passwordoff") as UIImage?
        
        if(iconClick == true) {
            showEye.setImage(imageOn, for: .normal)
            passwordTextField.isSecureTextEntry = false
            
        } else {
            showEye.setImage(imageOff, for: .normal)
            passwordTextField.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
    }
    
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
    }
    
    
    @IBAction func emailEditingChanged(_ sender: UITextField) {
        
        print("sender: \(sender.text!)")
        let isValidateEmail = self.validation.validateEmailId(emailID: sender.text!)
        if isValidateEmail == false {
            emailLabel.textColor = UIColor.red
            warningEmailLabel.textColor = UIColor.red
            warningEmailLabel.text = "La dirección de correo electrónico no es correcta"
            self.emailView.backgroundColor = UIColor.red
            
        }
        else{
            emailLabel.textColor = UIColor.black
            self.emailView.backgroundColor = UIColor.black
            warningEmailLabel.text = ""
        }
    }
    
    
    @IBAction func passwordEditingChanged(_ sender: UITextField) {
        
        print("sender: \(sender.text!)")
        
        if sender.text!.count >= 8 {
            //self.eyeButton.isHidden = true
            warningPasswordLabel.text = ""
            passwordLabel.textColor = UIColor.black
            self.passwordView.backgroundColor = UIColor.black
            
        }else {
            passwordLabel.textColor = UIColor.red
            warningPasswordLabel.textColor = UIColor.red
            warningPasswordLabel.text = "La contraseña no es correcta"
            self.passwordView.backgroundColor = UIColor.red
        }
        
    }
    
    func getDataProfilesList(){
        
        listProfileRequester.getListProfileData(){result in
            switch result{
            case .success(let listProfiles):
                self.arrayProfiles = listProfiles
                if (self.arrayProfiles.isEmpty == true){
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileDataViewController") as! ProfileDataViewController
                    yourVc.modalPresentationStyle = .fullScreen
                    self.present(yourVc, animated: true, completion: nil)
                }
                else{
                    
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                    yourVc.modalPresentationStyle = .fullScreen
                    self.present(yourVc, animated: true, completion: nil)
                }
                break;
            case .failure(_):
                let alert = UIAlertController(title: "", message: "¡Verifique su conexión a internet!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                self.present(alert, animated: true)
                break;
            }
            
        }
        
    }
    
    
    
    func validPayment(){
        
        print("--------------------------------------------------)")
        let userID = Auth.auth().currentUser!.uid
        print("userID: \(userID)")
        
        let docRef = db.collection("validateMonthlyPaymentByUser").document(userID)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                
                var isValid = true
                let serverTimestamp = document.get("timeStampEnd") as! String
                
                let date = NSDate(timeIntervalSince1970: Double(serverTimestamp)!)
                print("fecha: \(date)")
                
                let dateString = Date().toString(dateFormat: "dd-MM-yyyy")
                print("dateString is \(dateString)")
                
                let dateTod = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                
                let dateToday = formatter.string(from: dateTod)
                
                let dateServer = formatter.string(from: date as Date)
                
                if(dateServer <= dateToday){
                    
                    isValid = false
                    print("Tu suscripción ha vencido")
                    
                    let isVal = isValidPayment(isValid: isValid)
                    Singleton.shared.setIsValidPayment(name: isVal)
                    //self.validatePaymentObserver(isValidPayment: isValid)
                    
                }else{
                    isValid = true
                    print("Tu suscripción sigue vigente")
                    let isVal = isValidPayment(isValid: isValid)
                    Singleton.shared.setIsValidPayment(name: isVal)
                    //self.validatePaymentObserver(isValidPayment: isValid)
                }
                
                
                
            } else {
                let isValid = false
                print("Tu suscripción ha vencido")
                
                let isVal = isValidPayment(isValid: isValid)
                Singleton.shared.setIsValidPayment(name: isVal)
                print("Document does not exist")
            }
        }
        
        
    }
    
    private func validatePaymentObserver(isValidPayment: Bool){
        
        print("isValid: \(isValidPayment)")
        let userInfo = ["isValidPayment": isValidPayment]
        
        NotificationCenter.default.post(name: .validatePaymentByUser, object: nil, userInfo: userInfo)
        //self.dismiss(animated: true, completion: nil)
    }
    
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
