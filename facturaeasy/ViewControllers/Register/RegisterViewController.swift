

import Foundation
import UIKit
import Firebase


class RegisterViewController: UIViewController{
    
    var db: Firestore!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var showEye: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var warningEmailLabel: UILabel!
    @IBOutlet weak var warningNameLabel: UILabel!
    @IBOutlet weak var warningPasswordLabel: UILabel!
    
    @IBOutlet weak var viewWarningPassword: UIView!
    @IBOutlet weak var viewWarningName: UIView!
    @IBOutlet weak var viewWarningEmail: UIView!
    
    
    
    var validation = Validation()
    
    var iconClick = true
    
    override func viewDidLoad() {
        db = Firestore.firestore()
        //getCollectionDataFromFirestore()
        //getColletionWithDocs()
        passwordTextField.isSecureTextEntry = true
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        
    }
    
    func getColletionWithDocs(){
        
        let ref = db.collection("db_config").document("prod")
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                print(data["profileTypes"])
            } else {
                print("Couldn't find the document")
            }
        }
    }
    
    func getCollectionDataFromFirestore(){
        
        let collectionRef = db.collection("db_config")
        collectionRef.getDocuments { (querySnapshot, err) in
            if let docs = querySnapshot?.documents {
                for docSnapshot in docs {
                    print(docSnapshot.data())
                }
            }
        }
    }
    
    
    
    @IBAction func saveData(_ sender: UIButton) {
        
        let name = nameTextField.text
        let email = emailTextField.text
        let password = passwordTextField.text
        
        let isValidateEmail = self.validation.validateEmailId(emailID: email!)
        if (isValidateEmail == false) {
            print("Incorrect Email")
            warningEmailLabel.text = "Por favor introduce un email con formato válido"
            warningEmailLabel.textColor = UIColor.red
            return
        }
        
        if nameTextField.text == ""{
            warningNameLabel.text = "Por favor introduce Nombre y Apellidos"
            warningNameLabel.textColor = UIColor.red
            return
        }
        
        let isValidatePass = self.validation.validatePassword(password: password!)
        if (isValidatePass == false) {
            print("Incorrect Pass")
            warningPasswordLabel.text = "La contraseña debe tener al menos 8 caracteres"
            warningPasswordLabel.textColor = UIColor.red
            return
        }
        
        
        
        if (isValidateEmail == true || isValidatePass == true) {
            print("All fields are correct")
        }
        
        Auth.auth().createUser(withEmail: email!, password: password!) { (user, error) in
            if let e = error{
                print("hay un error", e)
                return
            }
            
            let userID = Auth.auth().currentUser!.uid
            print("userID: \(userID)")
            
            
            self.createUser(userId: userID)
            
            print("Registrado correctamente")
            let alert = UIAlertController(title: "", message: "¡Usuario registrado exitosamente!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                yourVc.modalPresentationStyle = .fullScreen
                self.present(yourVc, animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true)
            
            //callback?(nil)
        }
        
        
    }
    
    private func createUser(userId: String) {
        
        guard let nameAndLastname = nameTextField.text , let email = emailTextField.text , let password = passwordTextField.text , !nameAndLastname.isEmpty , !email.isEmpty , !password.isEmpty else {
            //simpleAlert(title: "Error", msg: "Debe completar todos los campos")
            return
            
        }
        
        let newUserReference = Firestore.firestore().collection("users").document(userId)    // <-- create a document, with the user id from Firebase Auth
        
        newUserReference.setData([
            "displayName": nameAndLastname,
            "email": email,
            "userId": userId
        ])
        
    }
    
    @IBAction func showEyeHide(_ sender: UIButton) {
        
        let imageOn = UIImage(named: "passwordon") as UIImage?
        let imageOff = UIImage(named: "passwordoff") as UIImage?
        
        if(iconClick == true) {
            showEye.setImage(imageOn, for: .normal)
            passwordTextField.isSecureTextEntry = false
            
        } else {
            showEye.setImage(imageOff, for: .normal)
            passwordTextField.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
        
    }
    
    @IBAction func editingPasswordChanged(_ sender: UITextField) {
        
        print("sender: \(sender.text!)")
        
        if sender.text!.count >= 8 {
            //self.eyeButton.isHidden = true
            warningPasswordLabel.text = ""
            passwordLabel.textColor = UIColor.black
            self.viewWarningPassword.backgroundColor = UIColor.black
            
        }else {
            passwordLabel.textColor = UIColor.red
            warningPasswordLabel.textColor = UIColor.red
            warningPasswordLabel.text = "La contraseña no es suficientemente segura. Usa al menos 8 caracteres entre letras y números"
            self.viewWarningPassword.backgroundColor = UIColor.red
        }
        
    }
    
    @IBAction func editingNameChanged(_ sender: UITextField) {
        print("sender: \(sender.text!)")
        if sender.text! != "" {
            //self.eyeButton.isHidden = true
            warningNameLabel.text = ""
            self.viewWarningName.backgroundColor = UIColor.black
            nameLabel.textColor = UIColor.black
        }else {
            nameLabel.textColor = UIColor.red
            warningNameLabel.textColor = UIColor.red
            warningNameLabel.text = "No puede dejar este campo en blanco"
            self.viewWarningName.backgroundColor = UIColor.red
        }
    }
    
    
    @IBAction func editingEmailChanged(_ sender: UITextField) {
        print("sender: \(sender.text!)")
        let isValidateEmail = self.validation.validateEmailId(emailID: sender.text!)
        if isValidateEmail == false {
            emailLabel.textColor = UIColor.red
            warningEmailLabel.textColor = UIColor.red
            warningEmailLabel.text = "Para continuar, introduce un correo electrónico válido"
            self.viewWarningEmail.backgroundColor = UIColor.red
            
        }
        else{
            emailLabel.textColor = UIColor.black
            self.viewWarningEmail.backgroundColor = UIColor.black
            warningEmailLabel.text = ""
        }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
