//
//  TypesDataModel.swift
//  facturaeasy
//
//  Created by Baxten on 16/10/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation


struct TypesDataModel{
    
    var types: [String]
    
}
