//
//  ListFactureDataModel.swift
//  facturaeasy
//
//  Created by Baxten on 16/10/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation


class ListFactureDataModel{
    
    var amount: String = ""
    var cardNumber: String = ""
    var cdfi: String = ""
    var company: String = ""
    var id: String = ""
    var invoiceValues: String = ""
    var paymentMethod: String = ""
    var retrievedText: String = ""
    var serverTimestamp: String = ""
    var status: String = ""
    var userId: String = ""
    var alias: String = ""
    var rfc: String = ""
    var selectedImagePosition: String = ""
    var profile: [String: Any]
    
    
    init(amount: String, cardNumber: String, cdfi: String, company: String, id: String, invoiceValues: String, paymentMethod: String, retrievedText: String, serverTimestamp: String, status: String, userId: String, profile: [String: Any]) {
        
        self.amount = amount
        self.cardNumber = cardNumber
        self.cdfi = cdfi
        self.company = company
        self.id = id
        self.invoiceValues = invoiceValues
        self.paymentMethod = paymentMethod
        self.retrievedText = retrievedText
        self.serverTimestamp = serverTimestamp
        self.status = status
        self.userId = userId
        self.profile = profile
        
    }
}

