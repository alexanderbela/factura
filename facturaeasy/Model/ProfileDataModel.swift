//
//  ProfileDataModel.swift
//  facturaeasy
//
//  Created by Baxten on 13/10/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation


class ProfileDataModel{
    
    var address: String = ""
    var alias: String = ""
    var defaultCdfi: Int = 0
    var defaultCompanyId: String = ""
    var defaultPaymentMethod: Int = 0
    var email: String = ""
    var idProfile: String = ""
    var lastNameFather: String = ""
    var lastNameMother: String = ""
    var municipality: String = ""
    var name: String = ""
    var phoneNumber: String = ""
    var postalCode: String = ""
    var rfc: String = ""
    var imagePosition: Int = 0
    var state: String = ""
    var type: Int = 0
    
    init(address: String, alias: String, defaultCdfi: Int, defaultCompanyId: String, defaultPaymentMethod: Int, email: String, idProfile: String, lastNameFather: String, lastNameMother: String, municipality: String, name: String, phoneNumber: String, postalCode: String, rfc: String, imagePosition: Int, state: String, type: Int) {
        
        
        
        self.address = address
        self.alias = alias
        self.defaultCdfi = defaultCdfi
        self.defaultCompanyId = defaultCompanyId
        self.defaultPaymentMethod = defaultPaymentMethod
        self.email = email
        self.idProfile = idProfile
        self.lastNameFather = lastNameFather
        self.lastNameMother = lastNameMother
        self.municipality = municipality
        self.name = name
        self.phoneNumber = phoneNumber
        self.postalCode = postalCode
        self.rfc = rfc
        self.imagePosition = imagePosition
        self.state = state
        self.type = type
        
    }
}
