//
//  PaymentDataRequest.swift
//  facturaeasy
//
//  Created by Baxten on 04/01/21.
//  Copyright © 2021 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

class CollectionsDataRequest {
    var db: Firestore!
    init() {
        
    }
    
    
    func getPaymentData(closure: @escaping(Result<PaymentDataModel, Error>)-> Void){
        db = Firestore.firestore()
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["paymentMethods"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["types"]
                    var array: [String] = []
                    for i in resultArray.arrayValue {
                        
                        let dato = i.stringValue
                        array.append(dato)
                        
                    }
                    
                    let fileid = PaymentDataModel(payment: array)
                    closure(.success(fileid))
                    //Singleton.shared.setPayment(name: fileid)
                    
                } catch
                {
                    closure(.failure(error))
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    
    func getCompaniesData(closure: @escaping(Result<CompaniesDataModel, Error>)-> Void){
        db = Firestore.firestore()
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["companies"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["companiesList"]
                    var array: [String] = []
                    
                    for i in resultArray.arrayValue {
                        
                        let dato = i["name"].stringValue
                        array.append(dato)
                        
                    }
                    
                    let fileid = CompaniesDataModel(companies: array)
                    closure(.success(fileid))
                    
                } catch
                {
                    closure(.failure(error))
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    
    func getCFDIData(closure: @escaping(Result<CFDIDataModel, Error>)-> Void){
        db = Firestore.firestore()
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["cdfi"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["types"]
                    var array: [String] = []
                    
                    for i in resultArray.arrayValue {
                        let dato = i.stringValue
                        array.append(dato)
                    }
                    
                    let fileid = CFDIDataModel(cfdi: array)
                    closure(.success(fileid))
                    
                } catch
                {
                    closure(.failure(error))
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    func getTypesData(closure: @escaping(Result<TypesDataModel, Error>)-> Void){
        db = Firestore.firestore()
        db.collection("db_config").document("prod").getDocument { (document, error) in
            if let document = document {
                let group_array = document["profileTypes"] as? [String:Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: group_array!, options: [])
                    let myresult = try? JSON(data: jsonData)
                    let resultArray = myresult!["types"]
                    var array: [String] = []
                    
                    for i in resultArray.arrayValue {
                        
                        let dato = i.stringValue
                        array.append(dato)
                        
                    }
                    
                    let fileid = TypesDataModel(types: array)
                    closure(.success(fileid))
    
                } catch
                {
                    closure(.failure(error))
                    print(error.localizedDescription)
                    
                }
                
            }
            
        }
    }
    
    
}
