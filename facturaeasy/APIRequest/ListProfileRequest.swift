//
//  ListProfileRequest.swift
//  facturaeasy
//
//  Created by Baxten on 31/12/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON


class ListProfileRequest {
    var db: Firestore!
    init() {
        
    }
    
    func getListProfileData(closure: @escaping(Result<[ProfileDataModelInvoices], Error>)-> Void){
        db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        var arrayListProfile = [ProfileDataModelInvoices]()
        self.db.collection("users").document(userID).collection("profiles").getDocuments { (snapshot, err) in
            
            
            if let err = err {
                closure(.failure(err))
                print("Error getting documents: \(err)")
            } else {
                for document in snapshot!.documents {
                    let direccion = document.get("address") as! String
                    let alias = document.get("alias") as! String
                    let defaultCdfi = document.get("defaultCdfi") as! Int
                    let defaultCompanyId = document.get("defaultCompanyId") as! String
                    let defaultPaymentMethod = document.get("defaultPaymentMethod") as! Int
                    let email = document.get("email") as! String
                    let id = document.get("id") as! String
                    guard let lastNameFather = document.get("lastNameFather") as? String else {continue}
                    guard let lastNameMother = document.get("lastNameMother") as? String else {continue}
                    let municipality = document.get("municipality") as! String
                    let name = document.get("name") as! String
                    let phoneNumber = document.get("phoneNumber") as! String
                    let postalCode = document.get("postalCode") as! String
                    guard let rfc = document.get("rfc") as? String else {continue}
                    let imagePosition = document.get("selectedImagePosition") as? Int
                    var pos = 0
                    if(imagePosition == nil){
                        pos = 0
                        
                        //self.imagePos.append(pos)
                    }else{
                        pos = imagePosition!
                        //self.imagePos.append(imagePosition!)
                    }
                    let state = document.get("state") as! String
                    let type = document.get("type") as! Int
                    
                    
                    let item = ProfileDataModelInvoices(address: direccion, alias: alias, defaultCdfi: defaultCdfi, defaultCompanyId: defaultCompanyId, defaultPaymentMethod: defaultPaymentMethod, email: email, idProfile: id, lastNameFather: lastNameFather, lastNameMother: lastNameMother, municipality: municipality, name: name, phoneNumber: phoneNumber, postalCode: postalCode, rfc: rfc, imagePosition: pos, state: state, type: type)
                    arrayListProfile.append(item)
                    
                }
                
                closure(.success(arrayListProfile))
               
            }
        }
    }
}
