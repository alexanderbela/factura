//
//  MenuRequestClass.swift
//  facturaeasy
//
//  Created by Baxten on 30/12/20.
//  Copyright © 2020 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

class MenuRequest {
    
    var db = Firestore.firestore()
    let userID = Auth.auth().currentUser?.uid
    
    init() {
        
    }
    
    func getDataFirebase(closure: @escaping(Result<[ListFactureDataModel], Error>)-> Void ){
        let dateFormatter = DateFormatter()
        let date = Date()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
        let startOfMonth = Calendar.current.date(from: comp)!
        var listFactureDataModelArray =  [ListFactureDataModel]()
        self.db.collection("users").document(userID!).collection("invoices").order(by: "serverTimestamp", descending: true).getDocuments { (snapshot, err) in
            
            if let err = err {
                //self.removeSpinner()
                closure(.failure(err))
                print("Error getting documents: \(err)")
            } else {

                for document in snapshot!.documents {
                    
                    let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                    let dateServer: Date = serverTimestamp.dateValue()
                    
                    if(dateServer >= startOfMonth){
                        
                        let amount = document.get("amount") as! String
                        let cardNumber = document.get("cardNumber") as! String
                        let cdfi = document.get("cdfi") as! String
                        guard let company = document.get("company") as? String else {continue}
                        let id = document.get("id") as! String
                        let invoiceValues = document.get("invoiceValues") as! String
                        let paymentMethod = document.get("paymentMethod") as! String
                        let retrievedText = document.get("retrievedText") as! String
                        let serverTimestamp = document.get("serverTimestamp") as! Timestamp
                        let dateServer: Date = serverTimestamp.dateValue()
                        let dateS = dateFormatter.string(from: dateServer)
                        let status = document.get("status") as! String
                        let userId = document.get("userId") as! String
                        let profileData = document.get("profile") as! [String: Any]
                        
                        let item = ListFactureDataModel(amount: amount, cardNumber: cardNumber, cdfi: cdfi, company: company, id: id, invoiceValues: invoiceValues, paymentMethod: paymentMethod, retrievedText: retrievedText, serverTimestamp: dateS, status: status, userId: userId, profile: profileData)
                        listFactureDataModelArray.append(item)
                        
                    }
                    
                }
                
                closure(.success(listFactureDataModelArray))
                
            }
        }
    }
    
}
