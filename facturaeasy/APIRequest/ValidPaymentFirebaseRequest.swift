//
//  ValidPaymentFirebaseRequest.swift
//  facturaeasy
//
//  Created by Baxten on 06/01/21.
//  Copyright © 2021 Alexander. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SwiftyJSON

class ValidPaymentFirebaseRequest {
    var db: Firestore!
    init() {
        
    }
    
    
    
    func getDataPaymentFirebase(closure: @escaping(Result<isValidPayment, Error>)-> Void){
        db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        let docRef = db.collection("validateMonthlyPaymentByUser").document(userID)
        var isValid = false
        docRef.getDocument { (document, error) in
            
            if let document = document, document.exists {
                
                
                let serverTimestamp = document.get("timeStampEnd") as! String
                let date = NSDate(timeIntervalSince1970: Double(serverTimestamp)!)
                let dateTod = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                
                let dateToday2 = formatter.string(from: dateTod)
                let dateServer2 = formatter.string(from: date as Date)
                
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                dateFormatter2.dateFormat = "dd-MM-yyyy"
                let dateToday = dateFormatter2.date(from:dateToday2)
                let dateServer = dateFormatter2.date(from:dateServer2)
                
                print("dateToday: \(dateToday!)")
                print("dateServer: \(dateServer!)")
                
                if(dateServer! <= dateToday!){
                    
                    isValid = false
                    print("Tu suscripción ha vencido")
                    let isVal = isValidPayment(isValid: isValid)
                    Singleton.shared.setIsValidPayment(name: isVal)
                    closure(.success(isVal))
                    
                }else{
                    
                    isValid = true
                    print("Tu suscripción sigue vigente")
                    let isVal = isValidPayment(isValid: isValid)
                    Singleton.shared.setIsValidPayment(name: isVal)
                    closure(.success(isVal))
                    
                }
                
            }
            else {
                
                if let err = error {
                    closure(.failure(err))
                    print("Error getting documents: \(err)")
                }
            }
        }
        let isVal = isValidPayment(isValid: isValid)
        closure(.success(isVal))
        
    }
    
    
    func requestData( closure: @escaping(_ arrayBreathalyzer: isValidPayment? , _ type: Int )-> Void  ) {
        db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        let docRef = db.collection("validateMonthlyPaymentByUser").document(userID)
        var isValid = false
        let isVal = isValidPayment(isValid: isValid)
        docRef.getDocument { (document, error) in
            print(error.debugDescription)
            if let document = document, document.exists {
                
                
                let serverTimestamp = document.get("timeStampEnd") as! String
                let date = NSDate(timeIntervalSince1970: Double(serverTimestamp)!)
                let dateTod = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                
                let dateToday2 = formatter.string(from: dateTod)
                let dateServer2 = formatter.string(from: date as Date)
                
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                dateFormatter2.dateFormat = "dd-MM-yyyy"
                let dateToday = dateFormatter2.date(from:dateToday2)
                let dateServer = dateFormatter2.date(from:dateServer2)
                
                print("dateToday: \(dateToday!)")
                print("dateServer: \(dateServer!)")
                
                if(dateServer! <= dateToday!){
                    
                    isValid = false
                    print("Tu suscripción ha vencido")
                    let isVal = isValidPayment(isValid: isValid)
                    
                    closure(isVal, 0)
                    
                }else{
                    
                    isValid = true
                    print("Tu suscripción sigue vigente")
                    let isVal = isValidPayment(isValid: isValid)
                    
                    closure(isVal, 1)
                    
                }
                
            }
            else {
                
                if let err = error {
                    closure(isVal, 2)
                    print("Error getting documents: \(err)")
                }
            }
        }
        
        
    }
}

