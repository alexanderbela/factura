

import UIKit
import Firebase
import SwiftyJSON

class ViewController: UIViewController {
    
    var db: Firestore!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var viewIniciar: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let auth = Auth.auth()
    var arrayProfiles = [ProfileDataModelInvoices]()
    var collectionsDataRequester = CollectionsDataRequest()
    var listProfileRequester = ListProfileRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        self.stateLogin()
    }
    
    func stateLogin(){
        
        self.activityIndicator.startAnimating()
        //self.showSpinner(onView: self.view)
        auth.addStateDidChangeListener { [weak self] (_, user) in
            
            if user != nil {
                
                self!.activityIndicator.stopAnimating()
                self!.getDataTypes()
                self!.getDataCFDI()
                self!.getDataMethods()
                self!.getDataCompaniesName()
                self!.view.sendSubviewToBack(self!.viewIniciar)
                self!.view.bringSubviewToFront(self!.loadingView)
                print("user is logged in")
                print("user already logged")
                
            } else {
                self!.activityIndicator.stopAnimating()
                self!.view.sendSubviewToBack(self!.loadingView)
                self!.view.bringSubviewToFront(self!.viewIniciar)
                //self!.activityIndicator.stopAnimating()
                print("user is not logged in")
                
            }
        }
    }
    
    @IBAction func initButton(_ sender: UIButton) {
        
        let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        yourVc.modalPresentationStyle = .fullScreen
        self.present(yourVc, animated: true, completion: nil)
        
    }
    
    func getDataProfilesList(){
        listProfileRequester.getListProfileData(){result in
            switch result{
            case .success(let listProfiles):
                self.arrayProfiles = listProfiles
                if (self.arrayProfiles.isEmpty == true){
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileDataViewController") as! ProfileDataViewController
                    yourVc.modalPresentationStyle = .fullScreen
                    self.present(yourVc, animated: true, completion: nil)
                }
                else{
                    let yourVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                    yourVc.modalPresentationStyle = .fullScreen
                    self.present(yourVc, animated: true, completion: nil)
                }
                break;
            case .failure(_):
                break;
            }
        }
    }
}

extension ViewController {
    func getDataTypes(){
        collectionsDataRequester.getTypesData(){ result in
            switch result{
            case .success(let fileid):
                Singleton.shared.setTypes(name: fileid)
                self.getDataProfilesList()
                break;
            case .failure(_):
                break;
            }
        }
    }
    
    func getDataCFDI(){
        collectionsDataRequester.getCFDIData(){ result in
            switch result {
            case .success(let fileid):
                Singleton.shared.setCFDI(name: fileid)
                break;
            case .failure(_):
                break;
            }
        }
        
    }
    
    func getDataMethods(){
        collectionsDataRequester.getPaymentData(){ result in
            switch result{
            case .success(let fileid):
                Singleton.shared.setPayment(name: fileid)
                break;
            case .failure(_):
                break;
            }
        }
    }
    
    func getDataCompaniesName(){
        collectionsDataRequester.getCompaniesData(){ result in
            switch result{
            case .success(let fileid):
                Singleton.shared.setCompanies(name: fileid)
                break;
            case .failure(_):
                break;
            }
        }
        
    }
    
}

