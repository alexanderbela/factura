
App Thinning Size Report for All Variants of facturaeasy

Variant: facturaeasy-152C9E86-F0C6-45BC-AE2B-D29DED0ED52D.ipa
Supported variant descriptors: [device: iPhone11,2, os-version: 13.0], [device: iPhone11,4, os-version: 13.0], [device: iPhone10,3, os-version: 13.0], [device: iPhone10,5, os-version: 13.0], [device: iPhone10,6, os-version: 13.0], [device: iPhone12,3, os-version: 13.0], [device: iPhone9,4, os-version: 13.0], [device: iPhone12,5, os-version: 13.0], [device: iPhone9,2, os-version: 13.0], [device: iPhone11,6, os-version: 13.0], [device: iPhone10,2, os-version: 13.0], and [device: iPhone8,2, os-version: 13.0]
App + On Demand Resources size: 10.5 MB compressed, 18.7 MB uncompressed
App size: 10.5 MB compressed, 18.7 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: facturaeasy-499AC9DB-B0EE-4820-81AA-42781DE50877.ipa
Supported variant descriptors: [device: iPad8,2, os-version: 13.0], [device: iPad8,7, os-version: 13.0], [device: iPad6,4, os-version: 13.0], [device: iPad11,2, os-version: 13.0], [device: iPad5,4, os-version: 13.0], [device: iPad7,1, os-version: 13.0], [device: iPad7,11, os-version: 13.0], [device: iPad11,3, os-version: 13.0], [device: iPad8,9, os-version: 13.0], [device: iPad5,3, os-version: 13.0], [device: iPad7,12, os-version: 13.0], [device: iPad8,6, os-version: 13.0], [device: iPad8,3, os-version: 13.0], [device: iPad8,4, os-version: 13.0], [device: iPad8,10, os-version: 13.0], [device: iPad6,12, os-version: 13.0], [device: iPad6,8, os-version: 13.0], [device: iPad11,4, os-version: 13.0], [device: iPad5,1, os-version: 13.0], [device: iPad7,5, os-version: 13.0], [device: iPad8,8, os-version: 13.0], [device: iPad11,1, os-version: 13.0], [device: iPad7,6, os-version: 13.0], [device: iPad7,3, os-version: 13.0], [device: iPad8,1, os-version: 13.0], [device: iPad5,2, os-version: 13.0], [device: iPad7,4, os-version: 13.0], [device: iPad7,2, os-version: 13.0], [device: iPad8,11, os-version: 13.0], [device: iPad8,5, os-version: 13.0], [device: iPad6,7, os-version: 13.0], [device: iPad6,11, os-version: 13.0], [device: iPad8,12, os-version: 13.0], and [device: iPad6,3, os-version: 13.0]
App + On Demand Resources size: 10.4 MB compressed, 18.7 MB uncompressed
App size: 10.4 MB compressed, 18.7 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: facturaeasy-704D245B-BAAF-42C6-A1D4-E4694AB41503.ipa
Supported variant descriptors: [device: iPhone12,1, os-version: 13.0] and [device: iPhone11,8, os-version: 13.0]
App + On Demand Resources size: 10.5 MB compressed, 18.7 MB uncompressed
App size: 10.5 MB compressed, 18.7 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: facturaeasy-820154C1-57D3-4502-8452-7DA024EBAADE.ipa
Supported variant descriptors: [device: iPhone12,8, os-version: 13.0], [device: iPhone8,4, os-version: 13.0], [device: iPhone10,4, os-version: 13.0], [device: iPod9,1, os-version: 13.0], [device: iPhone8,1, os-version: 13.0], [device: iPhone9,1, os-version: 13.0], [device: iPhone9,3, os-version: 13.0], and [device: iPhone10,1, os-version: 13.0]
App + On Demand Resources size: 10.4 MB compressed, 18.7 MB uncompressed
App size: 10.4 MB compressed, 18.7 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: facturaeasy.ipa
Supported variant descriptors: Universal
App + On Demand Resources size: 10.6 MB compressed, 18.9 MB uncompressed
App size: 10.6 MB compressed, 18.9 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed
